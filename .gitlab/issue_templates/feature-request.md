# About this template: 
- It is long. For a reason ;) 
- For an initial feedback round, please fill in the first non-drupal specific section, it is not necessary to point out all the details in the first round
- Please do not delete questions, rather leave empty or cross out if not applicable
- Please delete these instruction


:arrow_down: *The first section is not drupal specific*
# What is the overall goal

# What is/are the user story(s)?

# What is the business value?

# What is the feature concept?

# How can the functionality be tested?

## Anonymous users can

## Authenticated users can

## Editors can

## Group managers can

## User/platform management can


:arrow_down: *The second section is drupal specific*
# How does the feature logic map to Drupal objects?

# Does the feature affect the data model (changes/additions)?

# Which abstract permissions are affected (or need to be created)?

# What are technical crucial points?

# What effort does a sensible first sprint take?

# What effort can be estimated?

# On which site/instance will this be pioneered (+ issue link)?


:arrow_down: *Checks before start:*
# List of conceptual flaws to fix

# Do the effort estimates make sense?


:arrow_down: *Checks before done:*
# Works and committed on pioneer site's config-sync

# Feature committed in H4C feature-branch including:
## Feature concept committed
## Crucial points / resolutions / culdesacs docs committed
## Who-may-do-what / Abstractpermissions committed
## Testplan / behat committed
