# CHANGELOG

# 3.0.0

- Update hook todo:
  - deinstall enzian (what if we still need it on specific sites?)
  - deinstall drulma
  - deinstall drulma_companion
  - deinstall snippet_manager
  - deinstall block_class
  - install ui_patterns / ui_patterns_library / ui_patterns_field_group / ui_patterns_layout_builder
  - component_blocks
  - ui_patterns_settings
  - layout_builder_component_attributes
  - inline_details
  - redirect_404
  - views_infinite_scroll
  - smart_date
  - svg_formatter
  - filefield_to_mediafield
- Remove from composer.json
  - advagg
- Recommended drupal core version: 9.3.*
- New frontend theme dependency h4c_olive; almost all view modes are affected
- Migration required: h4c_article image is migrated to media image to add caption and credits: https://gitlab.com/geeks4change/sites/site-h4c-multi/-/issues/63
- Migration required: Each content type gets its own text format https://gitlab.com/geeks4change/sites/site-h4c-multi/-/issues/77
- Migration required: h4c_article hero image replaced h4c_multi_media image (the latter is used in organisations)
- h4c_search (and content type modules) search api views pages are being replaced with blocks embeddable into flexible pages
  - `/organisations`
  - `/events`
  - `/map`
  - `/articles`
- h4c_search facet source changes from views pages to a special block suffixed with `_facets`
- h4c_toolbar_menu changes gin_toolbar to use classic toolbar to avoid to much maintenance burdon
- h4c_core: Added new block types
  - card (pattern)
  - cta (pattern)
  - flexible block (lb-enabled)
  - html block
  - simple text
- Removed h4c_2level_menu
- h4c_event: Added event email and webform https://gitlab.com/geeks4change/sites/site-h4c-multi/-/issues/64


# Current version
- h4c_metatag: Simplify tokens to use site or node values or image from h4c_site_settings
- h4c_site_settings: Remove metatags paragraph and only add image field, bc title and slogan does not make sense here, add readme #449 e8f00a1
- h4c_core: Open ckeditor embedded image in lightbox cd5d12
- h4c_core: En and set defaults for diff module 0e27d9f5
- Hide revision field for all content types d53b01fc
- Disable node previews, which we do not support and add better node descriptions and help texts df11e5b5
- h4c_location: Add geocoding for location_geofield #470 a1be1607 3bcc3c7a

# 1.0.0-alpha9
- Introduce documentation system #468
- Clarify location ownership `Beta-blocker` #451
- Content moderation and content overview WTFs #411
- Better admin toolbar menu for platform managers (and other roles) #462 
- Add enduser docs #463 
- Turn on aggregation by default #448

# 1.0.0-alpha8
- Install prevent_unsafe_login 

# 1.0.0-alpha7
- Simplify media library #450
- Remove media types audio, iframe, media gallery, soundcloud #459
- (Re)move unused feature modules/config #460 / #459
- Minor group settings adjustments: Remove campaign group type / Remove location from orga/campaign groups
- Create basic term(topic/tags) pages #453
- Facet filters: Show vocabulary labels
- Replace most image styles with responsive image styles, simplify crop types #424

# 1.0.0-alpha6
- Use operation links instead of custom drop down in group content overview views #461 

# 1.0.0-alpha5
- Major update of user facing forms + gin as admin theme#440 / #430 
- Require enzian 2.0 #422 / https://gitlab.com/geeks4change/hubs4change/enzian/-/issues/267
- Update article list/ article preview view mode
- Update node view displays (especially full view)

# 1.0.0-alpha4
- Limit layout builder blocks #455

# 1.0.0-alpha3
- Removed h4c_campaign related default content from h4c_group_dc (dependencies were removed earlier)

## Details
* 9e476e0 dc: Remove h4c_campaign related group default content to make it installable without having h4c_campaign installed

# 1.0.0-alpha2
## TL;DR
- Introduced a lot of minor fixes and improvements
- Replace h4c_info_markup field and h4c_full_render view displays for event/organisation #426
- Updated documentation
- Check DO-ON-UPDATE.md before updating

### Regressions
- JS h4c_core: Remove JS which does weird things and only add back in if it is safe #433 (this was introduced for campaign pages to scroll to correct position if a url with anchor link is opened directly in the address bar)
- h4c_campaign cannot be installed because of a dependency error in h4c_simple_content_embed
  - This blocks group action links (optional config) to be installed because the view has a dependency on h4c_campaign #438

## Details
* 34b0bb6 docs: Update test docs with instructions to setup test system, update and simplyfy roadmap, update README.md #368
* 9e689d4 deps dc: Remove h4c_campaign(dc) so we can install recommended modules + dc without errors #429
* 8ba9741 config h4c_core: Set autload setting to false for modal entity_browsers #421
* 2ced4d1 deps: Add composer alias 1.0.x-dev to dev-master #436 / correct alias
* f7d8a20 deps: Add composer alias 1.0.x-dev to dev-master #436
* b56add3 dc: Remove all remains of info_markup field #394
* a7502c1 dc: Remove all remains of taxonomy_machine_name #394
* 79a9bf0 deps h4c_core: Add features because we have feature files in modules Update h4c_core.info.yml
* 97847a9 docs/deps: Augment admin_toolbar to 2.2, remove domain_assets module, add date_popup_datepicker patch and pin version #315
* 29c4d80 JS h4c_core: Remove JS which does weird things and only add back in if it is safe #433
* 2c8cb4d docs/config h4c_organisation: Add update removal hints, remove unwanted dependency from new view display
* 6e381ed Remove domain color patch and add forgotton update hint for #426
* 00f5498 config h4c_group h4c_event h4c_organistaion: Update block views, add simple card organisation view display and rename 16:9 card crop image type #432
* 3590ae6 Solve strange organisation preview display error appearing in Bonn #431
* 3dcfb3b Merge branch '426-short-info' into 'master'
* c404490 config h4c_organisation: Remove info_markup field and add field group list in full, preview and popup view displays #426
  * 8cf27767 docs h4c_core: Add docs for H4C Short infos item list #426
  * 6d2c7486 template h4c_location: Change order of templates geeks4change/sites/site-h4c-dev#32
  * 216559f5 Minor css improvements #426
  * 4653fe02 Not sorting out an issue with a not becoming active node_location_micro template in h4c_location #426
  * 34d5eb5c templates h4c_core, h4c_location: Add templates + CSS #426
  * 568db69c config h4c_location: Switch micro to content without ds columns #426
  * 9e83fa2f config h4c_event: Remove info_markup field and add field group list in full, teaser and popup view displays #426
  * b188b168 config h4c_organisation: Remove info_markup field and add field group list in full, preview and popup view displays #426
  * daae0304 require ds_chains #426
* 5e77f6f docs Update bin/README.md
* 941e869 Merge branch 'various-improvements' into 'master'
* 3d261d5 Various improvements
  * 0c5f1753 docs Fix anchor links in README.md #368
  * eaad8646 Add important links to README.md, restructure a bit, update ROADMAP.md #368
  * 846c763d Move view_modes_display to h4c_dev_tools and add some css #423
* daa52f5 Revert "config action h4c_sumoselect: Exclude content moderation from sumoselect selectors"
* 90c6761 css h4c_core: Add margin to summary on most full view modes
* 99ecd5a docs: Fix small issue in ROADMAP.md #368
* 12136b7 Update roadmap + remove blocker issues because they also live inside issues + add current feature projects #413
* c3c5a49 Update README.md with optional drupal core update step
* cfd3a28 Update test-docs #191
* 131b108 Merge branch 'master' of gitlab.com:geeks4change/hubs4change/h4c
* d377a61 Merge branch '417-use-agpl' into 'master'
* f1fe3df Merge branch '368-docs' into 'master'
* 9ababba docs: Update updating section in readme and knwon issues section in h4c_config_management readme #368
* 4666ef4 Use AGPL3+ license, fixes #417
* 2360760 config action h4c_sumoselect: Exclude content moderation from sumoselect selectors
* 364e81a deps h4c_core: Remove some dependendencies
* 6fe27fb Allow archived -> archived transition so the workflow of archived nodes is not accidentally set to draft https://gitlab.com/geeks4change/sites/site-h4c-freiburg/-/issues/136
* 1563478 Merge branch 'master' of gitlab.com:geeks4change/hubs4change/h4c
* d90695b js h4c_core: Disable anchor link behavior for nodes with photoswipe enabled gallery to avoid breaking photoswipe navigation #401
* c4d8119 lang: Make most fields translatable except for term fields because terms themselves shall be translated - translation is still not supported #399
* a8d069e lang: Set basic content language settings according to de/non alterable, translation allowed for most content types, non-translatable fields hidden #399
* 542b092 require: Remove unused modules from codebase #405
* 5812650 require: Remove menu_token module #404
* b19f4a5 config h4c_organisation/event: From Freiburg site: 1. Hide organisation opening hours text field from form and only show structured opening hours if not empty, set empty string as empty field behaviour for both fr-115 2. Add back event organiser field to full view and also show additional location info if no location is selected fr-126
* 24d3083 config h4c_core: Set first day of the week to monday #398
* 99779a9 Remove self-dependencies of modules added on feature export #400
* ee76c85 config h4c_organisation: Remove translation which does not have actual config and leads to WSOD on config import
* 47cb48e dep h4c_core: Remove layout_builder dependency
* 35d58ec dep h4c_core: Remove deprecated taxonomy_machine_name dependency
* 8b8ef62 config h4c_core: Remove accidentally added layout_builder dependencies from media config view displays