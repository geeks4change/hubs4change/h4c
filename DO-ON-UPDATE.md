# 3.0.x

- Remove snippet_manager ()

# 22.03.2021
- Make sure there is no data in term descriptions

- rm config-sync/views.view.h4c_taxonomy_reference.yml
- rm config-sync/search_api.index.content.yml
- rm config-sync/core.entity_view_display.node.event.search_index.yml


# 19.03.2021

- rm config-sync/filter.format.h4c_full_html.yml

# 15.03.2021 3

- /admin/structure/types/manage/article/fields/node.article.field_h4c_info_markup/delete

# 15.03.2021 2

rm config-sync/block.block.exposedformarticlepage_listing.yml
rm config-sync/block.block.h4c_article_topics.yml
rm config-sync/block.block.h4c_article_type.yml
rm config-sync/block.block.h4c_article_topics.yml
rm config-sync/block.block.h4c_article_type.yml
rm config-sync/block.block.exposedformh4c_eventslist.yml
rm config-sync/block.block.h4ceventseventtype.yml
rm config-sync/block.block.h4ceventsorganisations.yml
rm config-sync/block.block.h4ceventstags.yml
rm config-sync/block.block.h4ceventstopics.yml
rm config-sync/block.block.h4cmapeventtype.yml
rm config-sync/block.block.h4ceventstopics.yml
rm config-sync/block.block.exposedformh4c_organisationslist.yml
rm config-sync/block.block.h4cmapbusinesstypes.yml
rm config-sync/block.block.h4cmaporganisations.yml
rm config-sync/block.block.h4cmapsustainability.yml
rm config-sync/block.block.h4cmaptopics.yml
rm config-sync/block.block.h4corganisationsbusinesstypes.yml
rm config-sync/block.block.h4corganisationsorganisations.yml
rm config-sync/block.block.h4corganisationssustainability.yml
rm config-sync/block.block.h4corganisationstags.yml
rm config-sync/block.block.h4corganisationstopics.yml
rm config-sync/block.block.h4cmapbusinesstypes.yml
rm config-sync/block.block.h4cmapsustainability.yml
rm config-sync/block.block.h4cmaptopics.yml
rm config-sync/block.block.h4corganisationsbusinesstypes.yml
rm config-sync/block.block.h4corganisationssustainability.yml
rm config-sync/block.block.h4corganisationstopics.yml
rm config-sync/simple_block.simple_block.h4c_eventsmaplisttoggle.yml
rm config-sync/simple_block.simple_block.h4c_organisationsmaplisttoggle.yml
rm config-sync/simple_block.simple_block.h4c_eventsmaplisttoggle.yml
rm config-sync/simple_block.simple_block.h4c_organisationsmaplisttoggle.yml
rm config-sync/block.block.exposedformh4c_mapmap.yml
rm config-sync/block.block.h4c_eventsmaplisttoggle.yml
rm config-sync/block.block.h4c_organisationsmaplisttoggle.yml
rm config-sync/block.block.h4cfacets.yml
rm config-sync/block.block.h4cmaptags.yml
rm config-sync/block.block.h4cviewtoggle.yml
rm config-sync/blockgroup.block_group_content.h4c_facets.yml
rm config-sync/blockgroup.block_group_content.h4c_view_toggle.yml
rm config-sync/block.block.h4cfacets.yml

# 15.03.2021
- git diff --name-status --no-renames 1.0.0-alpha7 2.0.0-alpha1  |grep ^D

rm config-sync/field.field.config_pages.h4c_site_settings.field_h4c_metatags.yml
rm config-sync/field.storage.config_pages.field_h4c_metatags.yml

# 11.09.2020

rm config-sync/facets.facet.event_type.yml
rm config-sync/facets.facet.event_topics.yml
rm config-sync/language/de/pathauto.pattern.article_type.yml
rm config-sync/pathauto.pattern.article_type.yml
rm config-sync/views.view.taxonomy_term.yml
rm config-sync/language/de/pathauto.pattern.event_type.yml
rm config-sync/language/de/views.view.event.yml
rm config-sync/pathauto.pattern.event_type.yml
rm config-sync/views.view.event.yml
rm config-sync/language/de/pathauto.pattern.group.yml
rm config-sync/language/de/views.view.h4c_blocks.yml
rm config-sync/pathauto.pattern.group.yml
rm config-sync/pathauto.pattern.group_membership.yml
rm config-sync/pathauto.pattern.group_type.yml
rm config-sync/views.view.h4c_blocks.yml
rm config-sync/language/de/pathauto.pattern.group_content.yml
rm config-sync/pathauto.pattern.group_content.yml


# 10.09.2020

```
rm config-sync/core.entity_view_display.node.organisation.micro.yml
```

# 9.9.2020

- :exclamation: Remove files, BEFORE running updates (to deinstall twig_render_this)

```
rm config-sync/facets.facet.article_topics.yml
rm config-sync/facets.facet.article_type.yml
rm config-sync/core.entity_view_display.node.article.h4c_full_render.yml
rm config-sync/field.field.node.article.field_h4c_info_markup.yml
rm config-sync/language/de/field.field.node.article.field_h4c_info_markup.yml
rm config-sync/field.storage.node.field_h4c_info_markup.yml
```

# 6.08.2020 efa66bec

- Remove group type campaign

```
rm config-sync/group.content_type.group_content_type_42817fda61107.yml
rm config-sync/group.role.h4c_campaign_group-0b0baeca9.yml
rm config-sync/group.role.h4c_campaign_group-443ed8e03.yml
rm config-sync/group.role.h4c_campaign_group-54c37ab6b.yml
rm config-sync/group.role.h4c_campaign_group-a416e6833.yml
rm config-sync/group.role.h4c_campaign_group-admin.yml
rm config-sync/group.role.h4c_campaign_group-anonymous.yml
rm config-sync/group.role.h4c_campaign_group-b9f7429f0.yml
rm config-sync/group.role.h4c_campaign_group-ec70061de.yml
rm config-sync/group.role.h4c_campaign_group-f9295376f.yml
rm config-sync/group.role.h4c_campaign_group-member.yml
rm config-sync/group.role.h4c_campaign_group-outsider.yml
rm config-sync/group.type.h4c_campaign_group.yml
rm config-sync/language.content_settings.group_content.group_content_type_42817fda61107.yml
rm config-sync/language.content_settings.taxonomy_term.group_type.yml
rm config-sync/language/de/group.role.h4c_campaign_group-a416e6833.yml
rm config-sync/language/de/group.role.h4c_campaign_group-anonymous.yml
rm config-sync/language/de/group.role.h4c_campaign_group-member.yml
rm config-sync/taxonomy.vocabulary.group_type.yml
rm config-sync/core.entity_form_display.group.h4c_campaign_group.default.yml
rm config-sync/core.entity_form_display.group_content.group_content_type_c6548aeda68a4.default.yml
rm config-sync/core.entity_view_display.group_content.group_content_type_c6548aeda68a4.default.yml
rm config-sync/field.field.group_content.group_content_type_c6548aeda68a4.field_h4c_require_group_admin.yml
rm config-sync/field.field.group_content.group_content_type_c6548aeda68a4.group_roles.yml
rm config-sync/group.content_type.group_content_type_6fbe8969dad8a.yml
rm config-sync/group.content_type.group_content_type_7a4202e5cefac.yml
rm config-sync/group.content_type.group_content_type_80f5a14cee114.yml
rm config-sync/group.content_type.group_content_type_b141e7cb8f589.yml
rm config-sync/group.content_type.group_content_type_c6548aeda68a4.yml
rm config-sync/group.content_type.group_content_type_efbfcea9595fe.yml
rm config-sync/language.content_settings.group.h4c_campaign_group.yml
rm config-sync/language.content_settings.group_content.group_content_type_6fbe8969dad8a.yml
rm config-sync/language.content_settings.group_content.group_content_type_7a4202e5cefac.yml
rm config-sync/language.content_settings.group_content.group_content_type_b141e7cb8f589.yml
rm config-sync/language.content_settings.group_content.group_content_type_c6548aeda68a4.yml
rm config-sync/language.content_settings.group_content.group_content_type_efbfcea9595fe.yml
```

# 4.08.2020 / 2

```
rm config-sync/core.entity_view_display.node.article.box.yml
rm config-sync/core.entity_view_display.node.article.media.yml
rm config-sync/core.entity_view_display.node.article.rss.yml
rm config-sync/core.entity_view_display.node.event.media.yml
rm config-sync/core.entity_view_display.node.event.simple_card.yml
rm config-sync/core.entity_view_display.node.location.media.yml
rm config-sync/core.entity_view_display.node.page.card.yml
rm config-sync/core.entity_view_display.node.page.teaser.yml
rm config-sync/image.style.square_thumbnail.yml 
rm config-sync/image.style.small_square.yml 
```


# 4.08.2020 / 1

- See #460

## Removed from composer/pinnings

composer remove 
drupal/acb
drupal/anchor_link
drupal/audio_embed_field
drupal/color_field
drupal/crop
drupal/domain
drupal/genpass
drupal/geocoder
drupal/geolocation
drupal/image_widget_crop
drupal/media_entity_browser
drupal/media_entity_slideshow
drupal/media_entity_soundcloud
drupal/menu_per_role
drupal/single_datetime
drupal/smtp
drupal/taxonomy_manager
drupal/weight
drupal/workbench_email
drupal/video_embed_field
drupal/views_accordion
drupal/views_infinite_scroll
drupal/viewscss

## Removed config

```
## h4c_article
rm config-sync/field.field.node.article.comment.yml

## h4c_campaign
rm config-sync/core.entity_form_display.node.h4c_campaign.default.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.box.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.card.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.default.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.full.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.media.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.popup.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.rss.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.search_index.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.simple_card.yml
rm config-sync/core.entity_view_display.node.h4c_campaign.teaser.yml
rm config-sync/editor.editor.h4c_adv_html.yml
rm config-sync/facets.facet.h4c_campaign_campaign_types.yml
rm config-sync/facets.facet.h4c_campaign_topics.yml
rm config-sync/facets.facet.h4c_map_campaign_types.yml
rm config-sync/field.field.node.h4c_campaign.body.yml
rm config-sync/field.field.node.h4c_campaign.field_campaign_body.yml
rm config-sync/field.field.node.h4c_campaign.field_campaign_footer.yml
rm config-sync/field.field.node.h4c_campaign.field_campaign_footer_id.yml
rm config-sync/field.field.node.h4c_campaign.field_campaign_general.yml
rm config-sync/field.field.node.h4c_campaign.field_campaign_header.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_body.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_campaign_types.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_contact_emails.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_email.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_faq_types.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_featured.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_fixed_navbar.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_frontpage.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_imprint.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_logo_left.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_logo_left_url.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_logo_right.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_logo_right_url.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_menu_items.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_multi_media.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_phones.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_privacy.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_testimonial_help_text.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_webform.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_website.yml
rm config-sync/field.field.node.h4c_campaign.field_image.yml
rm config-sync/field.field.node.h4c_campaign.field_location_reference.yml
rm config-sync/field.field.node.h4c_campaign.field_meta_tags.yml
rm config-sync/field.field.node.h4c_campaign.field_subtitle.yml
rm config-sync/field.field.node.h4c_campaign.field_summary.yml
rm config-sync/field.field.node.h4c_campaign.field_tags.yml
rm config-sync/field.field.node.h4c_campaign.field_testimonial_types.yml
rm config-sync/field.field.node.h4c_campaign.field_topics.yml
rm config-sync/field.storage.node.field_campaign_body.yml
rm config-sync/field.storage.node.field_campaign_footer.yml
rm config-sync/field.storage.node.field_campaign_footer_id.yml
rm config-sync/field.storage.node.field_campaign_general.yml
rm config-sync/field.storage.node.field_campaign_header.yml
rm config-sync/field.storage.node.field_h4c_campaign_types.yml
rm config-sync/field.storage.node.field_h4c_contact_emails.yml
rm config-sync/field.storage.node.field_h4c_imprint.yml
rm config-sync/field.storage.node.field_h4c_logo_left.yml
rm config-sync/field.storage.node.field_h4c_logo_left_url.yml
rm config-sync/field.storage.node.field_h4c_logo_right.yml
rm config-sync/field.storage.node.field_h4c_logo_right_url.yml
rm config-sync/field.storage.node.field_h4c_menu_items.yml
rm config-sync/field.storage.node.field_h4c_privacy.yml
rm config-sync/filter.format.h4c_adv_html.yml
rm config-sync/language.content_settings.node.h4c_campaign.yml
rm config-sync/language.content_settings.taxonomy_term.h4c_campaign_types.yml
rm config-sync/field.field.node.h4c_campaign.body.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_body.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_contact_emails.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_email.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_faq_types.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_imprint.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_menu_items.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_multi_media.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_phones.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_privacy.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_webform.yml
rm config-sync/field.field.node.h4c_campaign.field_h4c_website.yml
rm config-sync/field.field.node.h4c_campaign.field_image.yml
rm config-sync/field.field.node.h4c_campaign.field_location_reference.yml
rm config-sync/field.field.node.h4c_campaign.field_meta_tags.yml
rm config-sync/field.field.node.h4c_campaign.field_subtitle.yml
rm config-sync/field.field.node.h4c_campaign.field_summary.yml
rm config-sync/field.field.node.h4c_campaign.field_tags.yml
rm config-sync/field.field.node.h4c_campaign.field_testimonial_types.yml
rm config-sync/field.field.node.h4c_campaign.field_topics.yml
rm config-sync/node.type.h4c_campaign.yml
rm config-sync/search_api.index.h4c_campaign.yml
rm config-sync/views.view.h4c_campaign_term_filters.yml
rm config-sync/webform.webform.campaign_contact.yml
rm config-sync/node.type.h4c_campaign.yml
rm config-sync/pathauto.pattern.h4c_campaign_types.yml
rm config-sync/pathauto.pattern.node_h4c_campaign.yml
rm config-sync/rdf.mapping.node.h4c_campaign.yml
rm config-sync/search_api.index.h4c_campaign.yml
rm config-sync/taxonomy.vocabulary.h4c_campaign_types.yml
rm config-sync/views.view.h4c_campaign_blocks.yml
rm config-sync/views.view.h4c_campaign_menu.yml
rm config-sync/views.view.h4c_campaign_term_filters.yml
rm config-sync/views.view.h4c_campaigns.yml
rm config-sync/webform.webform.campaign_contact.yml
rm config-sync/block.block.h4c_map_campaign_types.yml
rm config-sync/block.block.views_block__h4c_campaign_blocks_testimonial_help_text_block.yml
rm config-sync/block.block.views_block__h4c_campaign_menu_block_campaign_menu_campaign.yml
rm config-sync/block.block.views_block__h4c_campaign_menu_block_campaign_menu_portal.yml
rm config-sync/block_visibility_groups.block_visibility_group.h4c_campaign_listing.yml
rm config-sync/h4c_campaign.features.yml
rm config-sync/h4c_campaign.info.yml
rm config-sync/h4c_campaign.module

## h4c_comment
rm config-sync/comment.type.comment.yml
rm config-sync/core.entity_form_display.comment.comment.default.yml
rm config-sync/core.entity_view_display.comment.comment.default.yml
rm config-sync/core.entity_view_mode.comment.token.yml
rm config-sync/field.field.comment.comment.comment_body.yml
rm config-sync/field.storage.node.comment.yml
rm config-sync/language.content_settings.comment.comment.yml
rm config-sync/comment.type.comment.yml
rm config-sync/field.field.comment.comment.comment_body.yml
rm config-sync/rdf.mapping.comment.comment.yml
rm config-sync/h4c_comment.features.yml
rm config-sync/h4c_comment.info.yml

## h4c_core
rm config-sync/config_perms.custom_perms_entity.administer_site_information.yml
rm config-sync/crop.type.16_9.yml
rm config-sync/crop.type.1_1.yml
rm config-sync/crop.type.h4c_horizontal.yml
rm config-sync/crop.type.h4c_vertical.yml
rm config-sync/field.field.taxonomy_term.topics.field_h4c_image.yml
rm config-sync/field.field.taxonomy_term.topics.field_h4c_marker_icon.yml
rm config-sync/field.field.taxonomy_term.topics.field_h4c_term_color.yml
rm config-sync/field.storage.taxonomy_term.field_h4c_image.yml
rm config-sync/field.storage.taxonomy_term.field_h4c_marker_icon.yml
rm config-sync/field.storage.taxonomy_term.field_h4c_term_color.yml
rm config-sync/image.style.16_9.yml
rm config-sync/image.style.h4c_crop_horizontal_large.yml
rm config-sync/image.style.h4c_crop_horizontal_medium.yml
rm config-sync/image.style.h4c_crop_square_medium.yml
rm config-sync/image.style.h4c_crop_vertical_small.yml
rm config-sync/language.content_settings.crop.16_9.yml
rm config-sync/language.content_settings.crop.1_1.yml
rm config-sync/field.field.taxonomy_term.topics.field_h4c_image.yml
rm config-sync/field.field.taxonomy_term.topics.field_h4c_term_color.yml
rm config-sync/views.view.h4c_term_color_css_block.yml
rm config-sync/views.view.h4c_term_color_css_block.yml
rm config-sync/views.view.h4c_topics_list.yml
rm config-sync/block.block.views_block__h4c_term_color_css_block_term_color.yml
rm config-sync/workbench_email.workbench_email_template.h4c_node_add_draft.yml

## h4c_domain
rm config-sync/README.md
rm config-sync/rules.reaction.h4c_process_domains_to_add.yml
rm config-sync/core.entity_form_display.taxonomy_term.topics.default.yml
rm config-sync/field.field.taxonomy_term.topics.field_domains_to_add.yml
rm config-sync/field.storage.taxonomy_term.field_domains_to_add.yml
rm config-sync/h4c_domain.features.yml
rm config-sync/h4c_domain.info.yml

## h4c_faq
rm config-sync/workflows.workflow.editorial.yml
rm config-sync/core.entity_form_display.node.faq.default.yml
rm config-sync/core.entity_view_display.node.faq.box.yml
rm config-sync/core.entity_view_display.node.faq.card.yml
rm config-sync/core.entity_view_display.node.faq.default.yml
rm config-sync/core.entity_view_display.node.faq.full.yml
rm config-sync/core.entity_view_display.node.faq.media.yml
rm config-sync/core.entity_view_display.node.faq.rss.yml
rm config-sync/core.entity_view_display.node.faq.search_index.yml
rm config-sync/core.entity_view_display.node.faq.simple_card.yml
rm config-sync/core.entity_view_display.node.faq.teaser.yml
rm config-sync/field.field.node.faq.body.yml
rm config-sync/field.field.node.faq.field_faq_type.yml
rm config-sync/field.field.node.faq.field_faq_weight.yml
rm config-sync/field.field.node.faq.field_h4c_body.yml
rm config-sync/field.field.node.faq.field_h4c_frontpage.yml
rm config-sync/field.field.node.faq.field_meta_tags.yml
rm config-sync/field.storage.node.field_faq_type.yml
rm config-sync/field.storage.node.field_faq_weight.yml
rm config-sync/field.storage.node.field_h4c_faq_types.yml
rm config-sync/language.content_settings.node.faq.yml
rm config-sync/language.content_settings.taxonomy_term.faq_type.yml
rm config-sync/field.field.node.faq.body.yml
rm config-sync/field.field.node.faq.field_faq_weight.yml
rm config-sync/field.field.node.faq.field_meta_tags.yml
rm config-sync/node.type.faq.yml
rm config-sync/search_api.index.faq.yml
rm config-sync/views.view.faq.yml
rm config-sync/node.type.faq.yml
rm config-sync/pathauto.pattern.faq_type.yml
rm config-sync/pathauto.pattern.node_faq.yml
rm config-sync/rdf.mapping.node.faq.yml
rm config-sync/search_api.index.faq.yml
rm config-sync/taxonomy.vocabulary.faq_type.yml
rm config-sync/views.view.faq.yml
rm config-sync/block_visibility_groups.block_visibility_group.faq_listing.yml
rm config-sync/h4c_faq.features.yml
rm config-sync/h4c_faq.info.yml

## h4c_group
rm config-sync/config_perms.custom_perms_entity.administer_groups.yml
rm config-sync/views.view.media_entity_browser.yml

## h4c_organisation
rm config-sync/facets.facet.h4c_organisations_organisation_types.yml
rm config-sync/block.block.h4corganisationsorganisationtypes.yml

## h4c_people
rm config-sync/core.entity_form_display.node.people.default.yml
rm config-sync/core.entity_view_display.node.people.default.yml
rm config-sync/core.entity_view_display.node.people.full.yml
rm config-sync/core.entity_view_display.node.people.search_index.yml
rm config-sync/core.entity_view_display.node.people.small_card.yml
rm config-sync/core.entity_view_display.node.people.teaser.yml
rm config-sync/field.field.node.people.body.yml
rm config-sync/field.field.node.people.field_h4c_body.yml
rm config-sync/field.field.node.people.field_image.yml
rm config-sync/field.field.node.people.field_meta_tags.yml
rm config-sync/field.field.node.people.field_people_position.yml
rm config-sync/field.field.node.people.field_people_type.yml
rm config-sync/field.field.node.people.field_summary.yml
rm config-sync/field.storage.node.field_people_position.yml
rm config-sync/field.storage.node.field_people_type.yml
rm config-sync/language.content_settings.node.people.yml
rm config-sync/language.content_settings.taxonomy_term.people_type.yml
rm config-sync/field.field.node.people.body.yml
rm config-sync/field.field.node.people.field_image.yml
rm config-sync/field.field.node.people.field_meta_tags.yml
rm config-sync/field.field.node.people.field_summary.yml
rm config-sync/node.type.people.yml
rm config-sync/views.view.people.yml
rm config-sync/node.type.people.yml
rm config-sync/pathauto.pattern.people_node.yml
rm config-sync/pathauto.pattern.people_type.yml
rm config-sync/search_api.index.people.yml
rm config-sync/taxonomy.vocabulary.people_type.yml
rm config-sync/views.view.people.yml
rm config-sync/h4c_people.features.yml
rm config-sync/h4c_people.info.yml

## h4c_simple_content_embed
rm config-sync/core.entity_form_display.simple_content.h4c_article.default.yml
rm config-sync/core.entity_form_display.simple_content.h4c_article.h4c_embed.yml
rm config-sync/core.entity_form_display.simple_content.h4c_event.default.yml
rm config-sync/core.entity_form_display.simple_content.h4c_event.h4c_embed.yml
rm config-sync/core.entity_form_display.simple_content.h4c_faq.default.yml
rm config-sync/core.entity_form_display.simple_content.h4c_faq.h4c_embed.yml
rm config-sync/core.entity_form_display.simple_content.h4c_media_coverage.default.yml
rm config-sync/core.entity_form_display.simple_content.h4c_media_coverage.h4c_embed.yml
rm config-sync/core.entity_form_display.simple_content.h4c_testimonial.default.yml
rm config-sync/core.entity_form_display.simple_content.h4c_testimonial.h4c_embed.yml
rm config-sync/core.entity_form_mode.simple_content.h4c_embed.yml
rm config-sync/core.entity_view_display.simple_content.h4c_article.default.yml
rm config-sync/core.entity_view_display.simple_content.h4c_article.h4c_embed.yml
rm config-sync/core.entity_view_display.simple_content.h4c_article.h4c_embed__preview.yml
rm config-sync/core.entity_view_display.simple_content.h4c_event.default.yml
rm config-sync/core.entity_view_display.simple_content.h4c_event.h4c_embed.yml
rm config-sync/core.entity_view_display.simple_content.h4c_event.h4c_embed__preview.yml
rm config-sync/core.entity_view_display.simple_content.h4c_faq.default.yml
rm config-sync/core.entity_view_display.simple_content.h4c_faq.h4c_embed.yml
rm config-sync/core.entity_view_display.simple_content.h4c_faq.h4c_embed__preview.yml
rm config-sync/core.entity_view_display.simple_content.h4c_media_coverage.default.yml
rm config-sync/core.entity_view_display.simple_content.h4c_media_coverage.h4c_embed.yml
rm config-sync/core.entity_view_display.simple_content.h4c_media_coverage.h4c_embed__preview.yml
rm config-sync/core.entity_view_display.simple_content.h4c_testimonial.default.yml
rm config-sync/core.entity_view_display.simple_content.h4c_testimonial.h4c_embed.yml
rm config-sync/core.entity_view_display.simple_content.h4c_testimonial.h4c_embed__preview.yml
rm config-sync/core.entity_view_mode.simple_content.h4c_embed.yml
rm config-sync/core.entity_view_mode.simple_content.h4c_embed__preview.yml
rm config-sync/embed.button.h4c_simple_content_embed.yml
rm config-sync/entity_browser.browser.h4c_simple_content_browser.yml
rm config-sync/field.field.simple_content.h4c_article.field_cci_anchors.yml
rm config-sync/field.field.simple_content.h4c_article.field_h4c_placeholder.yml
rm config-sync/field.field.simple_content.h4c_event.field_cci_anchors.yml
rm config-sync/field.field.simple_content.h4c_event.field_h4c_placeholder.yml
rm config-sync/field.field.simple_content.h4c_faq.field_cci_anchors.yml
rm config-sync/field.field.simple_content.h4c_faq.field_h4c_placeholder.yml
rm config-sync/field.field.simple_content.h4c_media_coverage.field_cci_anchors.yml
rm config-sync/field.field.simple_content.h4c_media_coverage.field_h4c_placeholder.yml
rm config-sync/field.field.simple_content.h4c_testimonial.field_cci_anchors.yml
rm config-sync/field.field.simple_content.h4c_testimonial.field_h4c_placeholder.yml
rm config-sync/field.storage.simple_content.field_cci_anchors.yml
rm config-sync/field.storage.simple_content.field_h4c_placeholder.yml
rm config-sync/language.content_settings.simple_content.h4c_article.yml
rm config-sync/language.content_settings.simple_content.h4c_event.yml
rm config-sync/language.content_settings.simple_content.h4c_faq.yml
rm config-sync/language.content_settings.simple_content.h4c_media_coverage.yml
rm config-sync/language.content_settings.simple_content.h4c_testimonial.yml
rm config-sync/simple_content.simple_content_type.h4c_article.yml
rm config-sync/simple_content.simple_content_type.h4c_event.yml
rm config-sync/simple_content.simple_content_type.h4c_faq.yml
rm config-sync/simple_content.simple_content_type.h4c_media_coverage.yml
rm config-sync/simple_content.simple_content_type.h4c_testimonial.yml
rm config-sync/h4c_simple_content_embed.features.yml
rm config-sync/h4c_simple_content_embed.info.yml
rm config-sync/h4c_simple_content_embed.module

## h4c_testimonial
rm config-sync/workflows.workflow.editorial.yml
rm config-sync/core.entity_form_display.node.testimonial.default.yml
rm config-sync/core.entity_view_display.node.testimonial.box.yml
rm config-sync/core.entity_view_display.node.testimonial.card.yml
rm config-sync/core.entity_view_display.node.testimonial.default.yml
rm config-sync/core.entity_view_display.node.testimonial.full.yml
rm config-sync/core.entity_view_display.node.testimonial.media.yml
rm config-sync/core.entity_view_display.node.testimonial.rss.yml
rm config-sync/core.entity_view_display.node.testimonial.search_index.yml
rm config-sync/core.entity_view_display.node.testimonial.simple_card.yml
rm config-sync/core.entity_view_display.node.testimonial.teaser.yml
rm config-sync/field.field.node.testimonial.body.yml
rm config-sync/field.field.node.testimonial.field_h4c_featured.yml
rm config-sync/field.field.node.testimonial.field_h4c_frontpage.yml
rm config-sync/field.field.node.testimonial.field_h4c_testimonial_group.yml
rm config-sync/field.field.node.testimonial.field_image.yml
rm config-sync/field.field.node.testimonial.field_meta_tags.yml
rm config-sync/field.field.node.testimonial.field_summary.yml
rm config-sync/field.field.node.testimonial.field_testimonial_email.yml
rm config-sync/field.field.node.testimonial.field_testimonial_types.yml
rm config-sync/field.field.node.testimonial.field_testimonial_website.yml
rm config-sync/field.storage.node.field_h4c_testimonial_group.yml
rm config-sync/field.storage.node.field_h4c_testimonial_help_text.yml
rm config-sync/field.storage.node.field_testimonial_email.yml
rm config-sync/field.storage.node.field_testimonial_types.yml
rm config-sync/field.storage.node.field_testimonial_website.yml
rm config-sync/language.content_settings.node.testimonial.yml
rm config-sync/language.content_settings.taxonomy_term.testimonial_types.yml
rm config-sync/field.field.node.testimonial.body.yml
rm config-sync/field.field.node.testimonial.field_h4c_featured.yml
rm config-sync/field.field.node.testimonial.field_h4c_frontpage.yml
rm config-sync/field.field.node.testimonial.field_image.yml
rm config-sync/field.field.node.testimonial.field_meta_tags.yml
rm config-sync/field.field.node.testimonial.field_summary.yml
rm config-sync/field.field.node.testimonial.field_testimonial_email.yml
rm config-sync/field.field.node.testimonial.field_testimonial_types.yml
rm config-sync/field.field.node.testimonial.field_testimonial_website.yml
rm config-sync/field.storage.node.field_h4c_testimonial_group.yml
rm config-sync/views.view.testimonial.yml
rm config-sync/node.type.testimonial.yml
rm config-sync/pathauto.pattern.node_testimonial.yml
rm config-sync/pathauto.pattern.testimonial_types.yml
rm config-sync/rdf.mapping.node.testimonial.yml
rm config-sync/search_api.index.testimonial.yml
rm config-sync/taxonomy.vocabulary.testimonial_types.yml
rm config-sync/views.view.testimonial.yml
rm config-sync/block_visibility_groups.block_visibility_group.testimonial_listing.yml
rm config-sync/core.base_field_override.node.testimonial.title.yml
rm config-sync/h4c_testimonial.features.yml
rm config-sync/h4c_testimonial.info.yml
rm config-sync/h4c_testimonial.module

## h4c_user
rm config-sync/core.entity_view_display.user.user.compact.yml
rm config-sync/field.field.user.user.field_h4c_organisations_request.yml
rm config-sync/field.storage.user.field_h4c_organisations_request.yml
rm config-sync/field.field.user.user.field_h4c_organisations_request.yml
```


# 2.08.2020 d5c95432
- #450 #459

```
rm config-sync/simple_block.simple_block.h4c_top_anchor.yml
rm config-sync/core.entity_form_display.media.h4c_iframe.default.yml
rm config-sync/core.entity_form_display.media.h4c_iframe.media_library.yml
rm config-sync/core.entity_form_display.media.h4c_media_gallery.default.yml
rm config-sync/core.entity_form_display.media.h4c_media_gallery.media_library.yml
rm config-sync/core.entity_form_display.media.media_audio_embed.default.yml
rm config-sync/core.entity_form_display.media.media_audio_embed.media_library.yml
rm config-sync/core.entity_form_display.media.media_soundcloud_embed.default.yml
rm config-sync/core.entity_form_display.media.media_soundcloud_embed.media_library.yml
rm config-sync/core.entity_view_display.media.h4c_iframe.default.yml
rm config-sync/core.entity_view_display.media.h4c_iframe.h4c_embed.yml
rm config-sync/core.entity_view_display.media.h4c_iframe.media_library.yml
rm config-sync/core.entity_view_display.media.h4c_media_gallery.default.yml
rm config-sync/core.entity_view_display.media.h4c_media_gallery.full.yml
rm config-sync/core.entity_view_display.media.h4c_media_gallery.h4c_embed.yml
rm config-sync/core.entity_view_display.media.h4c_media_gallery.h4c_preview.yml
rm config-sync/core.entity_view_display.media.h4c_media_gallery.media_library.yml
rm config-sync/core.entity_view_display.media.media_audio_embed.default.yml
rm config-sync/core.entity_view_display.media.media_audio_embed.full.yml
rm config-sync/core.entity_view_display.media.media_audio_embed.h4c_embed.yml
rm config-sync/core.entity_view_display.media.media_audio_embed.h4c_preview.yml
rm config-sync/core.entity_view_display.media.media_audio_embed.media_library.yml
rm config-sync/core.entity_view_display.media.media_audio_embed.slick.yml
rm config-sync/core.entity_view_display.media.media_image.full.yml
rm config-sync/core.entity_view_display.media.media_soundcloud_embed.default.yml
rm config-sync/core.entity_view_display.media.media_soundcloud_embed.full.yml
rm config-sync/core.entity_view_display.media.media_soundcloud_embed.h4c_embed.yml
rm config-sync/core.entity_view_display.media.media_soundcloud_embed.h4c_preview.yml
rm config-sync/core.entity_view_display.media.media_soundcloud_embed.media_library.yml
rm config-sync/core.entity_view_display.media.media_soundcloud_embed.slick.yml
rm config-sync/embed.button.h4c_media_audio_embed.yml
rm config-sync/embed.button.h4c_media_gallery_embed.yml
rm config-sync/embed.button.h4c_media_iframe_embed.yml
rm config-sync/embed.button.media_entity_embed.yml
rm config-sync/entity_browser.browser.h4c_media_audio_browser.yml
rm config-sync/entity_browser.browser.h4c_media_audio_browser_modal.yml
rm config-sync/entity_browser.browser.h4c_media_gallery_browser.yml
rm config-sync/entity_browser.browser.h4c_media_iframe_browser.yml
rm config-sync/entity_browser.browser.media_entity_browser.yml
rm config-sync/entity_browser.browser.media_entity_browser_modal.yml
rm config-sync/field.field.media.h4c_iframe.field_h4c_html.yml
rm config-sync/field.field.media.h4c_iframe.field_media_image_1.yml
rm config-sync/field.field.media.h4c_media_gallery.field_h4c_media_help_text.yml
rm config-sync/field.field.media.h4c_media_gallery.field_h4c_media_references.yml
rm config-sync/field.field.media.media_audio_embed.field_media_audio_embed_field.yml
rm config-sync/field.field.media.media_soundcloud_embed.field_media_soundcloud.yml
rm config-sync/field.storage.media.field_h4c_html.yml
rm config-sync/field.storage.media.field_h4c_media_help_text.yml
rm config-sync/field.storage.media.field_h4c_media_references.yml
rm config-sync/field.storage.media.field_media_audio_embed_field.yml
rm config-sync/field.storage.media.field_media_image_1.yml
rm config-sync/field.storage.media.field_media_soundcloud.yml
rm config-sync/field.storage.node.field_h4c_fixed_navbar.yml
rm config-sync/language.content_settings.media.h4c_iframe.yml
rm config-sync/language.content_settings.media.h4c_media_gallery.yml
rm config-sync/language.content_settings.media.media_audio_embed.yml
rm config-sync/language.content_settings.media.media_soundcloud_embed.yml
rm config-sync/core.entity_view_mode.paragraph.h4c_multiple.yml
rm config-sync/entity_browser.browser.media_entity_browser.yml
rm config-sync/entity_browser.browser.media_entity_browser_modal.yml
rm config-sync/field.field.media.h4c_iframe.field_h4c_html.yml
rm config-sync/media.type.h4c_iframe.yml
rm config-sync/media.type.h4c_media_gallery.yml
rm config-sync/media.type.media_audio_embed.yml
rm config-sync/media.type.media_soundcloud_embed.yml
rm config-sync/simple_block.simple_block.h4c_top_anchor.yml
rm config-sync/core.entity_view_display.node.event.preview.yml
rm config-sync/field.field.node.event.field_h4c_fixed_navbar.yml
rm config-sync/field.field.node.event.field_h4c_fixed_navbar.yml
rm config-sync/group.content_type.group_content_type_19962e8002294.yml
rm config-sync/group.content_type.group_content_type_37c389828cdda.yml
rm config-sync/group.content_type.group_content_type_3d51153052db1.yml
rm config-sync/group.content_type.group_content_type_402a8a3e1911e.yml
rm config-sync/group.content_type.group_content_type_d1fb323840fed.yml
rm config-sync/group.content_type.group_content_type_ece6f1fdd2a05.yml
rm config-sync/language.content_settings.group_content.group_content_type_19962e8002294.yml
rm config-sync/language.content_settings.group_content.group_content_type_37c389828cdda.yml
rm config-sync/language.content_settings.group_content.group_content_type_3d51153052db1.yml
rm config-sync/language.content_settings.group_content.group_content_type_402a8a3e1911e.yml
rm config-sync/language.content_settings.group_content.group_content_type_d1fb323840fed.yml
rm config-sync/language.content_settings.group_content.group_content_type_ece6f1fdd2a05.yml
rm config-sync/block.block.enzian_group_operations.yml
rm config-sync/group.content_type.group_content_type_01a33846f2965.yml
rm config-sync/group.content_type.group_content_type_2425d3734d7e8.yml
rm config-sync/group.content_type.group_content_type_3974401f008a0.yml
rm config-sync/group.content_type.group_content_type_39864a8c58825.yml
rm config-sync/group.content_type.group_content_type_531a1e4d0d961.yml
rm config-sync/group.content_type.group_content_type_a1664f4901a1d.yml
rm config-sync/language.content_settings.group_content.group_content_type_01a33846f2965.yml
rm config-sync/language.content_settings.group_content.group_content_type_2425d3734d7e8.yml
rm config-sync/language.content_settings.group_content.group_content_type_3974401f008a0.yml
rm config-sync/language.content_settings.group_content.group_content_type_39864a8c58825.yml
rm config-sync/language.content_settings.group_content.group_content_type_531a1e4d0d961.yml
rm config-sync/language.content_settings.group_content.group_content_type_a1664f4901a1d.yml
rm config-sync/group.content_type.group_content_type_dc278f0e7f6b9.yml
rm config-sync/language.content_settings.group_content.group_content_type_dc278f0e7f6b9.yml
rm config-sync/views.view.h4c_groups.yml
rm config-sync/field.field.node.organisation.field_h4c_fixed_navbar.yml
rm config-sync/field.field.node.organisation.field_h4c_fixed_navbar.yml
rm config-sync/views.view.h4c_list.yml
rm config-sync/search_api_autocomplete.search.h4c_list.yml
rm config-sync/language/de/entity_browser.browser.media_entity_browser.yml
rm config-sync/language/de/entity_browser.browser.media_entity_browser_modal.yml
rm config-sync/language/de/field.field.media.h4c_iframe.field_media_image_1.yml
rm config-sync/language/de/views.view.h4c_list.yml

```

# 31.08.2020
## H4C 1.0.x / Enzian 2.0.x
- composer require geeks4change/h4c:1.0.x-dev geeks4change/enzian:2.0.x-dev
- run pinnings (because of config filter/ignore error)
- If you can't drush cr do mkdir web/themes/contrib/enzian/src/layout
- Go to /admin/appearance and install gin/h4c_gin/drulma, uninstall octavia/bulma
- drush updb
- Update cssvars.enzian.yml manually (var names have changed)
- Update block visibility group node title manually
- Enable ds field templates /admin/structure/ds/settings

### rm config

```
rm config-sync/block.block.enzian_account_menu.yml
rm config-sync/block.block.enzian_branding.yml
rm config-sync/block.block.enzian_group_operations.yml
rm config-sync/block.block.enzian_local_actions.yml
rm config-sync/block.block.enzian_local_tasks.yml
rm config-sync/block.block.enzian_main_menu.yml
rm config-sync/block.block.h4ctopanchor.yml
rm config-sync/block.block.views_block__group_nodes_block_draft.yml
rm config-sync/block.block.h4cmapcontenttype.yml
rm config-sync/block.block.h4cmaporganisationtypes.yml
rm config-sync/block.block.h4corganisationsorganisationtypes.yml
rm config-sync/language/de/block.block.enzian_account_menu.yml
rm config-sync/language/de/block.block.enzian_branding.yml
rm config-sync/language/de/block.block.enzian_local_tasks.yml
rm config-sync/language/de/block.block.enzian_main_menu.yml
rm config-sync/language/de/views.view.group_content.yml
rm config-sync/language/de/views.view.group_nodes.yml
rm config-sync/views.view.group_content.yml
rm config-sync/views.view.group_nodes.yml
```

### Notes
- sidebar_first breaks grid layout (e.g. if used on organisations, the whole orga grid is wrapped in col 3)

# 18.04.2020
- BEFORE: `drush pmu domain_assets`

# 16.04.2020
- en ds_chains
- removed files:
```
rm config-sync/core.entity_view_display.node.event.h4c_full_render.yml
rm config-sync/core.entity_view_display.node.event.preview.yml
rm config-sync/field.field.node.event.field_h4c_info_markup.yml
rm config-sync/core.entity_view_display.node.organisation.h4c_full_render.yml
rm config-sync/field.field.node.organisation.field_h4c_info_markup.yml
rm config-sync/language.content_settings.crop.h4c_horizontal.yml
rm config-sync/language.content_settings.crop.h4c_vertical.yml
rm config-sync/language/de/field.field.node.organisation.field_h4c_organisation_help_text.yml
```
- Other removed files since alpha1:
```
rm config-sync/language.content_settings.crop.h4c_horizontal.yml
rm config-sync/language.content_settings.crop.h4c_vertical.yml
rm config-sync/language/de/field.field.node.organisation.field_h4c_organisation_help_text.yml
```

# 09.03.2020
- Make sure you do not have photoswipe enabled gallery AND anchor links on the same page, H4C behavior for anchor links is disabled for those pages, see https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/401

# 06.03.2020
- Require modules in your local composer.json which have been removed upstream or deinstall unused modules, see https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/405

# 20.02.2020 - alpha1
En modules requied by h4c_core:
- en dropzonejs_eb_widget
- en ersv

# 16.02.2020
- `drush pmu h4c`:
  - We do not need an empty module in module root
  - `features_ui` is already a dependency of `h4c_devtools`

# 26.01.2020
- `drush en cssvars`
- for ie11 support:
  - `drush en cssvars_polyfill` and 
  - `mkdir -p libraries/css-vars-ponyfill && wget https://cdn.jsdelivr.net/npm/css-vars-ponyfill@2 -O libraries/css-vars-ponyfill/css-vars-ponyfill.js
- Set colors in `/admin/appearance/cssvars`

# 25.01.2020
- Optional: Import optional config if needed using features
- Optional: Delete old blocks

# 26.10.2019
- Optional after config import h4c_core: en h4c_simple_content_embed
- Add embed button to editor

# 11.10.2019
- BEFORE config import: `drush en media_entity_slideshow`
- Import `h4c_group/config/install/views.view.media_entity_browser.yml` through `/admin/config/development/features/diff`
- Use `h4c_core/assets/multiimage.png` in `/admin/config/content/embed/button/manage/h4c_media_gallery_embed`
- Remove multimediaheader block

# 08.10.2019
- Enable h4c_organisation_types_conditional module otherwise your organisation types' taxonomies are not conditional anymore, see h4c#300
