# About Inline docs 

## TODO
- Update to reflect latest changes in H4C
- Figure out how to distinguish between H4C inline docs and site docs
- Figure out where inline doc lives (in each module, for sites in a custom module?)
- Move this document to inline docs system once ready

## How to inline docs
- CA = config action
- Important information to state inline:
  - admin path; type of config (if empty `config/install`); comment
  - At least note that a settings page *has* customizations
  - State what it does if useful (in most cases yes)
  - State if there are problems, wtfs, plans to change
  - Link to more info issue/wiki/etc if exists (only public resources?!)
- Strategy: 
  - For H4C: Currently I am going through each modules config folders and write stuff down
  - For sites: It would be useful to have get a diff between H4C and site config
- Language:
  - Use english for H4C and other public modules
  - Use german for sites (?!)

# Inline documentation

## h4c_core
- includes css/js/templates/config
- adds form classes in `h4c_core.module`

### /admin/config/user-interface/extlink 
- CA: Has custom settings

### /admin/config/people/accounts
- CA: Require admin approval on registration

### /admin/config/user-interface/easy-breadcrumb
- CA: 
  - Show invalid path segments as non-links (e.g. if de `/topics` path alias translation is missing)
  - Don't include title segment, e.g. for topic term pages we cannot set the title dynamically (page manager token) #453

### /admin/config/regional/settings
- CA: Use monday as first day of week (`1`)

### /admin/config/people/accounts
- CA: Turn on visitors_admin_approval

### /admin/structure/views/settings
- CA: Show advanced column + master display

### /admin/config/media/blazy
- TODO: Review settings and move to CA

### /admin/structure/block/block-content/types
- H4C includes h4c_basic
- TODO: Review which lock types we need when looking at #468

### /admin/config/regional/date-time + /admin/config/regional/date_range_format
- TODO: Check which we need and delete those we don't

### /admin/config/content/embed
- TODO: Simplify usage of external media #443
- TODO: Use core media embed + media library, see #321
- TODO: Simplify media document file embed usage #447
- Unused upstream config: Node

### /admin/config/content/entity_browser
- modal browsers are needed for media entity reference fields
- non-modal (iframe) for ckeditor embeds where we are already inside a modal
- :exclamation: entity_browser does not check media permissions for upload forms: [Upload Widgets do not have any access-check [#3045286] | Drupal.org](https://www.drupal.org/project/entity_browser/issues/3045286)
- TODO: Use core media embed + media library #321 (deprecates all media entity browsers)
- TODO: Simplify media document file embed usage #447

### /admin/structure/display-modes/view + /admin/structure/display-modes/form
- See #423

### /admin/config/workflow/workflows/manage/editorial
- Don't allow users to use draft_from_published transition unless it is clear to them that the content they edit remains published #411
  - => :warning: Unless the permission for this transition is set, nobody is allowed to create drafts from existing content

### /admin/config/content/formats/manage/h4c_code
- Introduced for snippet_manager to avoid line breaks in code (h4c_search: snippet_manager.snippet.h4c_search_snippet.yml)

## h4c_flexible_page

### /admin/structure/types/manage/h4c_flexible_page/display

- Blocks are allowed per `/admin/config/block_list_override/settings` delivered with h4c_lb_addons module

## h4c_group

### /admin/structure/views/view/h4c_manage_group

- D9: If a group permission check is enabled (access group_node overview), the local webserver stops without a drupal error: The process has been signaled with signal "9". Found in https://gitlab.com/geeks4change/site-h4c-dev-local/-/issues/26

## h4c_lb_addons

### /admin/config/block_list_override/settings
- We only allow a few blocks to be used in lb, this is a global list and not content type specific

## h4c_search

### /admin/structure/snippet/h4c_search_snippet
- Includes all facet filter + exposed views filter + map/list toggle
- Facets need title enabled, the block title inside the snippet can't be used because we don't have translation
- Some parts are only used in enzian

## h4c_toolbar_menu

### /admin/config/user-interface/toolbar-menu/elements
- H4C Toolbar menu includes h4c_toolbar_menu
  - Uses `h4c-toolbar-menu` menu for editors/user managers/platform managers
  - Uses toolbar_menu module to add it to the admin toolbar
  - Uses toolbar_menu_clean module to remove drupal's admin toolbar menu for all roles except admins

### /admin/structure/menu/manage/h4c-toolbar-menu
- Menu items are added via `*.links.menu.yml` files or code in `h4c_toolbar_menu.module` file programmatically

#### /admin/structure/views/view/h4c_content
- `page`: Global content overview for editors, per content type overview lists are provided by modules

#### /admin/structure/views/view/h4c_content_moderated
- `page`: Global moderated content overview (except archived) for editors
- `archived`: Archived content overview for editors

## h4c_term_pages (experimental)

### /admin/structure/page_manager/manage/h4c_taxonomy_term_page/page_variant__h4c_taxonomy_term_page-layout_builder-0__layout_builder
- Hardcoded organisation views block label with initiative filter bc page manager does not support translation of lb yet
- TODO: Try to translate via config
