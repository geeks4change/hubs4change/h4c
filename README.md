# Hubs4Change: A Drupal 8 Distribution to engage communities

- 2.0.x branch = Drupal 9 compatible

## Documentation

- Online: https://hubs4change.readthedocs.io (or in `docs` directory)
- Developer docs: https://hubs4change.readthedocs.io/de/latest/developer/developer-docs/

## Proudly built by Geeks4Change

Geeks4Change is a sustainability activist Drupal collective. If you want to work with us or join as, contact us via [geeks4change.net](https://www.geeks4change.net).

