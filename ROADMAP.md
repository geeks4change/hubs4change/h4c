# Hubs4Change Roadmap

# Beta blocker
- Clarify role of and strategy of H4C in light of upcoming G4C strategy
- All reduce-pain projects

# Projects
- See https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/452

## Explanation

- :no_entry_sign: = Blocks another issue
- :no_entry: = Is blocked by
- :construction_worker: = Workable issue
- :information_source: = Status
  - `Active`
  - `Inactive`
  - `Not ordered yet`
  - `Implemented`
  - `Closed`
- Roadmap = Midterm plan pointing out priorities
- Sprintplan = Shortterm plan (for implementation)
- Issue queue policy: Open issues = actively worked on (issues will be closed after 4 weeks of inactivity)

### H4C Ideas

<details>

(from GeekCon Nov 2019)

- ~~strikethough~~ = Outdated or listed above
- ~~H4C DSGVO Haken (1T) - gesetzlich notwendig~~
- Minimale Anmeldefunktion (1T merlin) - hilft als RMZ Mitgliedschafts Anreiz
- Minimale Karten-Liste (1T merlin) - hilft für ux + kvm feature parity
- H4C RSS-Feed (1 Sitebuilder-Woche)
- H4C Kalender UX Verbessern (2 Sitebuilder-Wochen)
- Karte verbessern (???) (4 JS-Wochen)
- ~~ical Export (1 Sitebuilder-Woche)~~
- H4c Article / Blogfunktion (1 Sitebuilder-Woche)
- ~~Simplenews aufräumen (1 Sitebuilder-Woche)~~
- Freiburg API (4 @geek-merlin + 4 Sitebuilder-Wochen)
- H4C Workspace Merge Tool (API) (4 @geek-merlin + 4 Sitebuilder-Wochen)
- ~~Freiburg Upstream + Update (2 Sitebuilder-Wochen)~~
- H4C Cleanup + bugfixes (???) (4 @geek-andi-Wochen)
- H4C embed API Verbessern (2 @geek-andi + 2 @geek-merlin-Wochen)
- ~~H4C Umstellung organic groups (4 Sitebuilder + 4 @geek-merlin-Wochen)~~
- ~~H4C config-update Verbessern (2 Sitebuilder + 2 geek-merlin-Wochen)~~
- ~~Documentation Cleanup (1 Woche vorbereiten)~~
- Facets auf Views umstellen (1 Sitebuilder-Woche + 1 geek-merlin-Woche)
- H4C benutzerfreundlicher machen (???) (2 Team-Wochen)
- UX/Accessibility Redesign (8 Themer-Wochen)
- iframe* ()
- ~~repeating dates* ()~~
- ~~DSGVO konsequenter umsetzen (1 geek-rainer-Woche)~~
- ~~Datenschutz Sitebuilding (2 Sitebuilder-Wochen)~~
- ~~Gitlab labels: Guidelines + implementation~~
- ~~Develop policy for module css / vs theme css + implement~~

</details>
