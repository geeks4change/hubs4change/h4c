# About Test-docs

### Todo
- [ ] Update tests

- Each test is built like this: 
```
## drupal user role/abstract permission can
- view/add/create/edit/click/toggle/... something: after colon desired result if applicable
```
- Wording: add = already exists, create = does not yet exist
- Later roles inherit earlier roles (from top to bottom) 
  - except for * only inherits auth user permissions

## Possible roles/abstract permissions

- Anonymous users can
- Authenticated users can
- Group admins can
- Content management can
- User management can*
- Platform management can

# Test-docs

# H4C Article
- Todo

# H4C Core
## Anonymous users can
- register via `/user/register` and get confirmation on admin approval
- receive a one-time-login via `/user/password` when user account exists
- login via `/user`

## Authenticated users can
- /

## Content management can
- create/view/edit draft nodes under `/admin/content/moderated`
- create/view/edit all group content under `/admin/content`
- manage taxonomies
- see admin toolbar
- overwrite metatag settings per node
- embed media items into h4c_body fields (depends on content type text format)
  - :warning: embedded images use their own caption field which currently cannot be mapped to a media field, see [[media entities] Prepopulate caption with Media field data [#3052239] | Drupal.org](https://www.drupal.org/project/entity_embed/issues/3052239)

## User management*
- receive email notification when user sends group membership request form
- create user accounts in `/admin/people/create`
- receive email notification when user account is created
- activate/block user accounts
- add/remove users to an organisation group
- delegate roles under `/user/uid/roles`
- edit other user profiles
- view other users webform submissions

## Platform management can
- :exclamation: create iframe content #381
- :exclamation: save blocks in layout builder while in source mode without data loss ([upstream](https://www.drupal.org/project/drupal/issues/3095304))

## (TODO: Manage site
- manage blocks (not placement)
- manage url aliases
- manage metatag defaults
- manage user account settings
- edit emails

# H4C Domain
- TODO

# H4C Event

## Anonymous users can
- create a draft event in editorial group


# H4C Group

## Group admins can 
- :exclamation: view and revert group content revisions
  - see [Access to Revisions of Group Content is Broken [#3256998] | Drupal.org](https://www.drupal.org/project/group/issues/3256998)

## Plattform managers can
- edit content in any group
  - see changes in group 2.0 https://www.drupal.org/list-changes/group

# H4C Location

## Anonymous users can
- create a published location through organisation/event node form
- insert address which is automatically geo-coded

# H4C Metatag
- Todo

# H4C Multilingual

## Content management can
- :exclamation: translate content #418

# H4C Organisation
## Anonymous users can
- view a message to login/register before requesting group membership on organisation pages
- create an draft organisation group node
  - :exclamation: media can't be added to group on node creation #416

## Authenticated users can
- view a webform to request group membership on each organisation page

## Group admins can
- view group action links on user page/organisation node
  - title/logo (only user page)
  - view organisation node (only user page)
  - edit organisation node
  - create article
  - create event
  - view members
  - view content
- view/edit content in group (all moderation states)
  - :exclamation: drafts are not visible to group admins https://gitlab.com/geeks4change/sites/site-h4c-freiburg/issues/120
  - event
  - article
  - location
  - page
  - media
- add all media items created on groupcontent nodes as groupcontent
- embed media / create media entities on embed in ckeditor
  - image
    - full width
    - preview
    - medium
    - :exclamation: with link/without external linking by user (not possible atm, see #292)
  - video
  - file
  - soundcloud
  - external audio file
  - media gallery
    - full width 3-col
- overwrite some fields of embedded media (title, alt, alignment and caption)
- edit the media entity itself using edit link to target=_blank
- cannot see admin toolbar

## Content management
- receive email notification for newly created draft organisations

# H4C Page
- Todo

# H4C Search
## Anonymous users can

### Toggle block (/map, /organisations and /event)
- view map/list toggle on the right on /map, /events, /organisations
- toggle back and forth: Land on the initial url (view #822)
- activate filters and toggle back and forth: (1) land on the initial url (2) have the correct filter applied

### Filters (/map, /organisations and /event)
- click on filters
  - Text
  - Event type (sumo dropdown)
  - Organizer (sumo dropdown)
  - Tags (sumo dropdown)
  - Topics (checkboxes)
  - Business Types (checkboxes)
  - Sustainability (checkboxes)
- click on autocomplete suggestions in text filter on typing X characters (**fails, no autocomplete**)
- click on filter heading closes, click again opens

#### Only /map
- view all markers for events/organisations
- click on a marker: Popup opens on the left
  - click on topics, business types, sustainability terms: Filtered map view shows
  - click on title, subtitle, image, view profile link: Node opens
  - click on spamspan protected email
  - click on website: Link opens in new tab/window

# H4C Simple_content_embed
- Todo

# H4C Testimonial
- Todo

# Todo or otherwise not ready for test-docs

# H4C Comment (not in use)
# H4C FAQ (deprecated in current state)
# H4C Flexible Page (Experimental)
# H4C People (not in use)
# H4C User (not in use)