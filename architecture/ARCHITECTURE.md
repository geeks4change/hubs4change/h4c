# Architecture overview of H4C 3.0.x

Once the architecture is solid, update INLINE-DOCS.md, TEST-DOCS.md, README.md and move *this* to `/docs`

# General

- Template [geeks4change/h4c_template](https://gitlab.com/geeks4change/hubs4change/h4c_template)
  - requires Profile
  - inlcudes libraries
  - updates need to be manually brought to site repos
- Profile: [hubs4change](https://gitlab.com/geeks4change/hubs4change/hubs4change)
  - requires 
    - h4c (Module suite)
    - h4c_olive (Theme)
  - installs
    - h4c_core
    - h4c_multilingual
      - :warning: Unclear why exactly
    - h4c_olive (theme)
    - h4c_gin (theme)
- H4C Module suite [h4c](https://gitlab.com/geeks4change/hubs4change/h4c)
  - requires
    - h4c_gin
      - :warning: Why is it required in h4c but installed in profile?
    - all module dependencies including version pinnings
  - depends on h4c_olive
- H4C Olive [h4c_olive](https://gitlab.com/geeks4change/hubs4change/h4c_olive) 
  - Includes all patterns used in H4C
- H4C Media [h4c_media](https://gitlab.com/geeks4change/hubs4change/h4c_media)
  - Attempt to make h4c more modular, currently only includes new functionality
- H4C Migrate