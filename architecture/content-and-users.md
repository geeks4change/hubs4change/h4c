# (WIP) H4C User

## Begriffe
- *User account*: Email+Login Name
  - *Als LoginName kannst du deinen Klarnamen oder ein Pseudonym verwenden. Irreführende Namen wie "Admin" oder Organisationsname sind nicht zulässig, und werden abgeändert oder gesperrt. Dein Namen ist nur für andere eingeloggte Menschen sichtbar, nicht für Suchmaschinen und nicht-eingeloggte.*
- *User Profile*: Einem User Account zugeordnete Entitäten
  - *Grundprofil*: Klarname, persönliche Telefonnummer
  - *Mitgliedschaftsprofil*: s.u.
  - Weitere: *Fach-Experte*, *Fördermitglied*, *Abonnent*

## Kontrakt
- Jedes Organisations-Mitglied hat zwingend:
  - *User account*
  - *Grundprofil*
  - *Mitgliedschafts-Profil*
    - :warning: Eventl nicht immer, z.B. für R&C
- Jeder *User account* wird per Email verifiziert

## Implementation notes
- Erstellung von *User account*, *Grundprofil* und *Mitgliedschaftsprofil* per Webform Assistent

# H4C Group

## Begriffe
- *Gruppe*: Entität+Rechtecontainer
- *Organisations-Gruppe*: 
  - Entität: Organisations-Seite
  - Rechtecontainer: Verwaltung von Organisation+Gruppeninhalten+Mitgliedern
  - Die Mitglieder der Orga-Gruppe bilden die Orga-Redaktion
- *Domain-Gruppe*: 
  - Entität: Domain-Startseite
  - Rechtecontainer: Verwaltung von Domain-Startseite+Gruppeninhalten+Mitgliedern
  - Die Mitglieder der Domain-Gruppe bilden die Domain-Redaktion
- *Organisations-Inhalte*: Termine, Artikel, Basic/Flexi pages
- *Mitgliedschafts-Profil*: Rolle, Büro-Telefon; Zuordnung zu Gruppe+User account

## Kontrakt
- *Vorschläge für neue Inhalte* können nur der Domain-Redaktion gemacht werden
  - Warum nicht auch jeder beliebigen Organisation? Wir müssten wissen, welche Organisation aktiv ist und Vorschläge verarbeitet

# H4C Organisation Group

## Kontrakt
- Jede kann *Organisations-Gruppen* vorschlagen
- Jede kann *Vorschläge zur Aktualisierung* von Organisationsinhalten einreichen
- Jeder Organisations-Inhalt gehört zu *einer* Gruppe

### Organisation wird gleichzeitig mit User account erstellt
- *Organisations-Mitgliedschaft* eindeutig

#### Variante: User account existiert schon
- *Jemand hat unter deiner E-Mail eine Organisation auf XXX angelegt. (Ja ich war das.) (Nein, bitte löschen.)*
  - Nach N mal versenden dieser Mail wird der Vorgang in die "Keine-Antwort" Moderationsliste eskaliert.
- Nach Bestätigen der Mail wird der Vorgang in die "Organisaiton prüfen" Moderationsliste eskaliert.
- Mod schaltet frei, kann dabei die Bestätigungsmail noch anpassen.
- Member1 wird *Organisations-Admin* (Rolle).

### Organisation OHNE Mitglied wird von User account geclaimt
- Bsp: Organisations-Gruppe wird von anonym vorgeschlagen; anderer user account claimt Organisation
- :question: Wie stellen wir fest, dass User account von Organisation autorisiert ist?
  - Einschätzung liegt im Kompetenz-Bereich der Domain-Redaktion (Recherche, Nachfrage, Vertrauen)
- => Domain-Redaktion legt Organisations-Mitgliedschaft an

### Organisation MIT Mitglied wird von User account geclaimt
- Anfrage wird an Orga-Redaktion weitergeleitet
- => Orga-Redaktion legt Organisations-Mitgliedschaft an

### Verantwortlichkeiten
- Jedes *Organisations-Mitglied* kann Rolle des VISDP selber einnehmen (aber nicht zuweisen), i.d.R. erstes Mitglied
  - Jedes *Organisations-Mitglied* (oder: nur VISDP) kann weitere Gruppenmitglieder einladen
  - Jedes *Organisations-Mitglied* (oder: nur VISDP) kann Inhalte ohne Moderation erstellen+aktualisieren
  - Jedes *Organisations-Mitglied* (oder: nur VISDP) kann *Vorschläge* annehmen oder ablehnen
  - Domain-Redaktion greift nur in Hoheit ein, wenn Plattform Regeln missachtet werden
- *Organisations-Mitglied* gibt Rolle des VISDP wieder ab
  - Anfrage an übrige Gruppen-Mitglieder: *Deine Gruppe hat kein VISDP mehr, daher wurden alle Inhalte von der Veröffentlichung zurück genommen. Um die Gruppeninhalte wieder zu veröffentlichen, trage dich als VISDP ein. Falls niemand VISDP ist, entscheidet die Domain-Redaktion, ob sie die Inhalte unter ihrer Hoheit erneut veröffentlicht.*
- Wenn kein VISDP: 
  - Domain Redaktion kann VISDP annehmen
  - Wenn sie es tut: 
    - Aktualisierung von Organisationsinhalten nur als *Vorschläge* an Domain-Redaktion
    - Neue Gruppeninhalte als Vorschläge an Domain-Redaktion
    - Einladen weiterer Organisations-Mitglieder nicht möglich (weil nicht sinnvoll)
  - Wenn sie es nicht tut: Inhalte werden archiviert
- :warning: Benötigt die eindeutige Zuordnung jeder Organisation zu einer Domain Gruppe

## Fragen
- Wer darf Revisionshistorie einsehen?
- Wer darf sehen, wer was bearbeitet hat?

# User&Content create
- Erstellen von Organisations-Gruppe und Gruppeninhalte (optional) per Webform
- Bearbeitung aller Inhalte: Per normale Formulare

## Erstellen der Organisations-Gruppe

### Anonymes User
- Webform-Wizard zur Erstellung von
  - Basis-Organisations-Daten (Mit Prüfung "Existiert Organisation schon?")
  - Optional für Gruppenverwaltung:
    - *Account*
    - *Grundprofil*
    - *Gruppenmitgliedschaft* als Admin+VISDP
- Wizard *Organisation vorschlagen*
  - Auswahlfeld: Existierende Organisation (*Gibt es deine Organisation schon? Wenn ja, kannst du die Mitgliedschaft beantragen*)
    - Wenn Orga vorhanden: Weiter zu Wizard *Organisation claimen* (s.o.)
  - Name + Kurzbeschreibung der Organisation
    - *Möchtest du Gruppen-Admin + VISDP werden und das Organisations-Profil weiter ausfüllen?* => Weiter zu *User Account Wizard* / Link zum Bearbeiten Formular per Email
    - *Möchtest du weitere Angaben zur Organisation machen?* => Weiter *Organisations-Profil Wizard*
    - *Möchtest du anonym bleiben und keine weiteren Angaben machen?* => Senden
- Wizard *Organisations-Profil*
  - Weitere Organisations-Felder
    - *Möchtest du Gruppen-Admin + VISDP werden und das Organisations-Profil weiter ausfüllen?* => Weiter zu *User Account Wizard*
    - *Möchtest du anonym bleiben und keine weiteren Angaben machen?* => Senden

### User ist eingeloggt
- Wie oben, jedoch:
  - *Möchtest du Gruppen-Admin + VISDP werden und das Organisations-Profil weiter ausfüllen?*
  - Führt zu *Gruppenmitgliedschaft* Formular

## Erstellen von Organisations-Inhalten
- Die Organisations-Auswahl des Erstellen-Webforms enthält:
  - Null oder mehr "Erstelle in meiner Gruppe" Auswahlen (User hat Gruppenmitgliedschaft)
  - Oder "Schlage vor" Auswahl für Domain-Gruppe
- Der Ablauf unterscheidet sich je nach Login- und Mitgliedschafts-Status:
  - Falls schon Mitglied
    - Webform wird sofort gemappt und gespeichert(?), weiter auf Bearbeiten-Seite.
  - Falls schon eingeloggt
    - Webform erscheint in Moderationsliste der *zuständigen Redaktion*.
    - im einfachsten Fall ist die zuständige Redaktion per Domain konfiguriert.
    - später kann die Zuteilung nach ausgewählten Themen zugeteilt werden.
  - Falls nicht eingeloggt
    - Webform fragt nach E-Mail (optional)
    - Einsendung als Vorschlag an Domain Redaktion
---
Ergänzung von Merlin
- Webform fragt "Login erstellen und nächstes mal einfacher vorschlagen", "eingeloggt bleiben", "auch ein Passwort erstellen (andernfalls kannst du dir später einen Login Link zuschicken lassen, oder das Passwort auch später setzen)"
- => weiter wie "Falls schon Mitglied"
- **Jedoch** Für Gruppen-Inhalte (nicht Gruppen-Entität) hat es nur einen Mehrwert eingeloggt zu sein, wenn User auch Gruppenmitgliedschaft hat
  - Ansatz: Vorschlagen so einfach wie möglich machen (gerade auch anonym) + Motivieren eigene Gruppe anzulegen oder Mitgliedschaft in bestehender zu beantragen

# Remind&Connect

## Kontrakt
- Benötigt *User account*
