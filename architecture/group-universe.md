# (Noch was zum Gruppen-Konzept, noch sehr vage)
- Aktueller Stand: Gruppen haben Profile, "Organisation" wird ein (verpflichtendes) Profil.
- Wilde Idee: Gruppen **sind** Profile einer Organisation. So kann eine Organisation eine Content-Gruppe oder eine "Universums"-Gruppe (s.u.) werden. (Merlin denkt nach...)
- So könnte die OrgGruppe verpflichtend sein, die "Editorial/SubSite/Universums" Gruppe optional.
- Die unter beschriebenen "Universums" Gruppen könnten zusätzlich dazu kommen.
- Vielleicht sollten die Mitglieder gleich sein (synchronisiert oder von einer Master-Gruppe geerbt), aber die Rollen per Gruppe verschieden?
- Oder ist es dann doch einfacher, per Gruppe zu konfigurieren, welche Rollen erlaubt sind?
- Ja, das ist ein Zwischenfazit: Es führt zum gleichen Ziel.
  - verschiedene Gruppentypen mit den gleichen Mitgliedern zu haben.
  - einen Gruppentyp mit konfigurierbar eingeschränkten Rollen zu haben.
- ABER: Ich muss das noch stärker durchdringen, was wie kombiniert werden kann.

# Weiterführend: Master of the Universes Konzept
- Ein Universum (klingt besser als SubSite) ist ein in sich abgeschlossener Teil einer H4C Instanz. Eines von:
  - Eine eigene Domain
  - Eine *Themenwelt* in einer Domain (sieht aus wie eine eigene Website, hat alle Links mit URL Präfix der zugehörigen Gruppe, und wird mit Themenwelt Header dargestellt)
  - Eine Einbettung
- Ein Universum enthält
  - Eine zugehörige Gruppe, sowie ihre Elterngruppe
  - Ein oder mehrere Menüs (ggf. nicht dargestellt)
  - Eigene Inhalte
  - Mit Flag versehene FremdInhalte
- Flags
  - Flags haben Checkboxen und ersetzen irgendwann Term Felder
  - Damit werden Inhalte uns ihre Zuordnung getrennt.
  - Inhalte werden in einer Moderationsliste (ggf. "Null") geflaggt
- Über Menüs
  - Menüswerden bei Domains und Themenwelten i.d.R. angezeigt (und sind ggf. verpflichtend)
  - Menüs werden bei Einbettungen nur bei Bedarf angezeigt
  - Menüs dienen in jedem Fall zur Abgrenzung: Teil einer Themenwelt ist, was im Menü ist.
  - Menüs enthalten zusätzlich unsichtbare Multi-Einträge mit Verweis auf ein parametrisiertes View Display.
  - Beispiel:
    - Ober-Eintrag: Link /group/garden/subcontent/events
    - Unter-Eintrag: Multi-Eintrag /group/garden/subcontent/events
    - Effekt: Termine der Liste sind Teil des Universums und erhalten "interne" Links
      - Bei Einbettungen: Bleibt im Iframe
      - Bei Themenwelten: "Bleibt in der Themenwelt", d.h. hat eine Link in der Themenwelt /group/garden/subcontent/event/gardenparty, und wird mit Themenwelt Header dargestellt.

# Master of the Universes Battle-Plan

## Meilenstein 1: Einbettungs-Universen
- Erstelle Menü Multi-Einträge und Breadcrumbs
- Mache Einbettung-Gruppen
- Mache Group Menüs mit GroupContentMenu
- Mache Logik zur Unterscheidung interne und externe Links in Einbettungen
- Setze die Ausgabe der /subcontent/ Links als Embed um
- zack feddich

## Hierarchie-Optionen
- Setze Eltern-Gruppe um
  - Wichtig, um Themenwelt auf richtiger Domain und mit richtigem Header darzustellen
- Setze Vererbung von Mitgliedern und Rechten um
  - Wichtig, falls Elterngruppen Mitglieder "Ordnungsgewalt" in Unter-Universen haben sollen.
  - Alternative: Aller Content einer Untergruppe ist auch Content der Elterngruppe (dann ist aber keine Unterscheidung der Rechte möglich, was für Mitglieder-Vererbung spricht weil flexibler)
  
## Meilenstein2 2: Themenwelten
- (Nutze Erreichtes von M1)
- Setze die Ausgabe der /subcontent/ Links mit Themenwelt Header um
- Mache Themenwelt-Gruppen
- zack feddich