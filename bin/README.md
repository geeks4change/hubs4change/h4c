# Files
* `h4c-drutopia-pinnings.sh` holds the drutopia versions which H4C is based on to allow us to update config coming from drutopia, see also https://gitlab.com/geeks4change/hubs4change/h4c/issues/101
* `h4c-tested-global-pinnings.sh`: Holds tested module versions (does not include drupal/core)
*  TODO:`h4c-dc-list.sh` Document what it is supposed to do

# Pinned drupal module versions

## How to use recommended module pinnings

* Find recommended (=tested) module versions in `bin/h4c-tested-global-pinnings.sh`
* Apply all by running `./vendor/bin/composer-pin-apply web/modules/contrib/h4c/bin/h4c-tested-global-pinnings.sh`

## Why we pin module versions
* On installing a site via composer latest allowed (=according to semantic versions in all composer.json files) dependencies are downloaded: That's what we do not want because there might be new aka untested versions which break the site
* Therefore we maintain the tested module versions in the `bin/h4c-tested-global-pinnings.sh`

## Common problems
### Why don't we require exact versions in H4C?
...like it is done by [drupal/core-recommended](https://github.com/drupal/core-recommended/blob/8.8.x/composer.json) introduced in 8.8
* We can't because we do not want to force specific versions onto sites but offer a set of working modules. If done like in the mentioned drupal package, a site couldn't ever use another version of a H4C dependency than required by H4C.

### I cannot update a module
* tl;dr: Use composer `remove drupal/webform` to unpin the module
* Long version:`composer update drupal/webform` won't do anything because we pinned the webform module version in *composer.json* to a specific version (instead of using semver which is what composer is made for. Composer would only use the *.lock* file which is being updated for webform using the update command. 

### Dependencies cannot be resolved
* tl;dr: Use composer `remove drupal/webform` to unpin the module (sometimes you need to do this for an accidentally pinned submodule as well)
* By pinning modules we restrict the range of possible version combinations. Therefore you might run into unresolved dependency errors e.g. on updating some of the modules

### Running `./vendor/bin/composer-pin-apply web/modules/contrib/h4c/bin/h4c-tested-global-pinnings.sh` fails 
* Try again
* Remove failing patches => Try again
* If patches fail com from upstream and cannot be removed, update module to version where patch applies => Try again

Other things you can try (**with caution!**):
* `rm -rf web/modules/contrib/*` => Try again
* `composer remove drupal/*` (might also remove dependencies not included in pinnings from composer root) => Try again

## Why are some modules pinned to commits? NEEDS UPDATE
- drupal/abstractpermissions              1.x-dev e00c8a2
  - latest
- drupal/block_class                      1.x-dev 71a7fb3 
  - see https://gitlab.com/geeks4change/hubs4change/h4c/issues/324#patches-needed
- drupal/ctools                           3.x-dev 060c7b1
  - dev after last release: https://www.drupal.org/project/ctools/issues/3104932 / might be because of patches used
- drupal/empty_fields                     1.x-dev a310b7c
  - latest
- drupal/gin                              3.x-dev#0eed516
  - See https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/473
- drupal/field_delimiter                  1.x-dev 2335e2c
  - latest
- drupal/hide_revision_field              2.x-dev 15ea589
  - latest
- drupal/groupmediaplus                   1.x-dev 90d1e14
  - latest
- drupal/markdown                         2.x-dev e06a3fb
  - dev after last release, see https://www.drupal.org/project/markdown/issues/2952435
- drupal/rules                            3.x-dev 78c9c13
  - on update check compatibility with https://gitlab.com/geeks4change/hubs4change/h4c_refpull
- geeks4change/h4c_gin                    1.0.x-dev#d2b44619
  - new toolbar css, tag new version when gin alpha28 is released