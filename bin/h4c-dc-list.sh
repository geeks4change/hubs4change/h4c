#!/bin/bash

for f in $1/content/*; do
    if [ -d "${f}" ]; then
        echo "${f}:"|sed -r "s|$1\/content\/||"   # your processing here
	ls "${f}"|sed -r 's/\.json//g'|sed 's/^/  \- /'
    fi
done
