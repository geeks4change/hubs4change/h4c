# Hubs4Change for developers

This is the _starting point_ for (new) developers.

## Where do I find docs?

### This guide
- General information about conventions, install and update instructions.

### Module readme
- ***Goal* and *scope* of a module in general**
- `module_dir/README.md`
- Contract-like information: What can other modules do with it?
- Includes todos and known issues (these are different from the known issues in the community guide)
- Links to relevant discussions in issue queues
- Linked from module description

### Module inline docs
- ***How* a module works in detail**
- Currently in `INLINE-DOCS.md`
- Documentation specific to config entities, which can be identified by path/route
- Will be displayed in Drupal UI perspectively

### Module test docs
- ***What* a module is supposed to do in detail**
- Currently in `TEST-DOCS.md`
- What functionality exactly is provided by a module?
- This will be the base of behat tests

## Where do I find the code?
- [Hubs4Change group on gitlab](https://gitlab.com/geeks4change/hubs4change)
    - Drupal template: [H4C Template](https://gitlab.com/geeks4change/hubs4change/h4c_template) (composer starting point to set up a site)
    - Drupal profile (aka distribution): [H4C Profile](https://gitlab.com/geeks4change/hubs4change/hubs4change) (minimal profile which only holds dependencies)
    - [H4C Module suite](https://gitlab.com/geeks4change/hubs4change/h4c) (main repo which holds all the feature modules)
    - Frontend theme: [Enzian](https://gitlab.com/geeks4change/hubs4change/enzian) (atm the only supported H4C theme)
    - Backend theme: [H4C Gin](https://gitlab.com/geeks4change/hubs4change/h4c-gin) (minor modifications of gin theme)

## When will H4C be ready for drupal 9?

Hubs4Change will be ready for drupal 9 before drupal 8 reaches EOL (November 2021), see [Drupal core release cycle: major, minor, and patch releases | Drupal.org](https://www.drupal.org/core/release-cycle-overview).

The update of existing sites to drupal 9 will be very smooth because drupal major version updates became very easy since 8.0, see [Making Drupal upgrades easy forever | Dries Buytaert](https://dri.es/making-drupal-upgrades-easy-forever).

## Why is some config namespaced and some is not?

Generally we namespace everything using a `h4c` prefix. 

For historical reasons not all our config is namespaced: Hubs4Change was built on top of Drutopia. Drutopia does not namespace config. While H4C does not depend anymore on drutopia, a lot of config has been taken from drutopia so is is not namespaced. 