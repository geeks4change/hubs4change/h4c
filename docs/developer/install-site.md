# Install a new site

## Requirements
- See [Drupal 8 requirements](https://www.drupal.org/docs/system-requirements)
- [Composer](https://getcomposer.org/doc/)

## Install H4C

1. Run `composer create-project geeks4change/h4c_template your-project --stability=dev`
1. `cd your-project`
1. Pin modules to tested versions: `./vendor/bin/composer-pin-apply web/modules/contrib/h4c/bin/h4c-tested-global-pinnings.sh`
1. Visit your site and run the installer (or do `drush si hubs4change`)
1. Install H4C modules to your liking (recommended: all stable modules)
1. Import missing config via `/admin/config/development/features`, select H4C bundle
1. Optionally: Import translations via `/admin/reports/translations/check`

## Install a local dev site (requires php >= 7.2/Ubuntu)

1. Do 1.-4. from above
1. `drush rs`
1. Do all other steps from 5. on
