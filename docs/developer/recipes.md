# Recipes

## Useful commands

- `mkdocs build -s` mkdocs, make build and check links

## Deployment with multilangng

- `composer install`
- `drush sqlq "delete from locales_target where customized>0"`
- `drush locale-import --type=customized de ../translations/custom/de.po`
- drush deploy

(Export de.po `drush locale:export --types=customized de > translations/custom/de.po`)

## Twig: Get an organisation + location reference and create a link

- Requires entity_twig module 

```
{% set organisation = entity_node['#node'].get('nid').value %}
{% if organisation %}
  {% set location = entity_load('node', organisation).field_location_reference.target_id %}
  <a href="/form/update-organisation?source_entity_type=node&source_entity_id={{ organisation }}&organisation={{ organisation }}{% if location %}&location={{ location }}{% endif %}" class="webform-dialog webform-dialog-normal" title="Schlage Änderungen vor."><i class="fas fa-edit"></i>{{ link_text }}</a>
{% endif %}
```