# Updating a site
## Minor update of drupal core

1. `composer update drupal/core "drupal/core-*" --with-all-dependencies`
1. `drush updb`
1. Export and commit new config

When not all `drupal/core*` modules are updated to the latest stable version, use `composer update drupal/core "drupal/core-*" --with-all-dependencies --with=drupal/core-recommended:9.X.0` to see which module causes the error

Find more information in [Update core via Composer | Drupal 8 guide on Drupal.org](https://www.drupal.org/node/2700999)

## Major update of drupal core

1. Edit composer.json to (all `drupal/core*` packages e.g. from ~8.9.0 => ~ 9.0.0)
1. `composer update drupal/core "drupal/core-*" --with-all-dependencies`
1. `drush updb`
1. Export and commit new config

## Update modules 

Don't forget to run `drush updb` after module updates. 

### According to pinnings

On each H4C update, you should check if the pinned versions have changed. To do so simply run the same script as on install:

- `./vendor/bin/composer-pin-apply web/modules/contrib/h4c/bin/h4c-tested-global-pinnings.sh`

### Remove project from composer root (and leave dependency requirement solely to upstream)

- Remove the pinning from composer root: `composer remove drupal/module`

### Update non-pinned modules

For modules that are not pinned via H4C pinnings, this is the usual composer/drupal procedure:

1. `composer update drupal/module`

## Update config of H4C (and other modules)

1. Update to new module (+theme) version, e.g. `composer update geeks4change/h4c geeks4change/enzian`
1. `drush updb`
1. Do what's described in [DO-ON-UPDATE.md](https://gitlab.com/geeks4change/hubs4change/h4c/-/blob/master/DO-ON-UPDATE.md) since your current version
1. Go to `/admin/config/development/configuration/distro`
1. Select modules, click import config
1. Export and commit new config

(See `Known issues` section in [H4C Config Management module readme](https://gitlab.com/geeks4change/hubs4change/h4c/-/blob/master/modules_dev/h4c_config_management/README.md))
