# Über diese Dokumentation
 
Diese Dokumentation richtet sich an drei Personengruppen:

- Redakteure mit mittlerer technischer Kompetenz
- Platform Manager (PM) mit hoher technischer Kompetenz
- Entwickler

<div class="admonition warning">
<p class="admonition-title">Achtung!</p>
<p>Plattform Manager können Dinge unwiderruflich kaputt machen.</p>
</div>

<div class="admonition warning">
<p class="admonition-title">Abweichung</p>
<p>bonn4future.de: Die Redaktion hat Plattform Manager Berechtigungen, hier ist also besondere Vorsicht geboten.</p>
</div>