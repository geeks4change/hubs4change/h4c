# Inhalte bearbeiten: Flexible Page

Mit Flexible Pages kannst du benutzerdefinierte Layouts und Inhalte erstellen.

## Grundlagen: Abschnitte (sections), Regionen und Blöcke (blocks)

Jede Seite kann aus beliebig vielen Abschnitten bestehen. Abschnitte erstrecken sich immer über die gesamte Breite. Abschnitte können unterschiedliche Anordnungen von Regionen beinhalten, meistens in Form von Spalten. In jede Region können beliebig viele Blöcke hinzugefügt werden. Diese Blöcke enthalten die eigentlichen Inhalte.

![Screenshot](content-flexible-pages-1.jpg)

## Fortgeschritten: Abschnitts-Einstellungen

Die Einstellungen eines Abschnitts kannst du bearbeiten, indem du auf `Configure (Abschnittsname)`, also im Screenshot "Configure Abschnitt 1" klickst. 

![Screenshot](content-flexible-pages-2.jpg)

### Die Einstellungen in der Übersicht:

"Field templates": Wird üblicherweise nicht benötigt.

"Variant": Anzahl der Spalten

"Wrapper":

- "Keine Auswahl": Der Abschnitt und dessen Inhalte nehmen die Standardbreite der Website ein und haben die Form einer Box. Wenn du eine Hintergrundfarbe einstellst, wird nur die Box ausgefüllt
- "Full centered": Der Abschnitt erstreckt sich über die gesamte Breite des Browserfensters, seine Inhalte jedoch nur über die Standarbreite der Website. So kannst du eine Hintergrundfarbe wählen, die sich über die gesamte Breite des Browserfensters erstreckt und den Abschnitt deutlich abhebt
- "Full width": Der Abschnitt und dessen Inhalte erstrecken sich über die gesamte Breite des Browserfensters

"Add top/bottom regions": Legt Regionen an, die sich über die gesamte Breite erstrecken. Z.B. praktisch bei mehrspaltigen Abschnitten, die eine gemeinsame Überschrift bekommen sollen.

"Two columns": Wenn du oben 2 Spalten ausgewählt hast, kannst du hier die Aufteilung der 2 Spalten einstellen. Zur Auswahl stehen:

- Keine Auswahl: 50:50 
- 66:33 
- 33:66

Weitere:

- "Region title above box": Legt zusätzliche Regionen in jeder Spalte an. Z.B. praktisch bei mehrspaltigen Abschnitten, die jeweils eine eigene Überschrift bekommen sollen. Meistens direkt oberhalb einer weißen Box.
- "Background color": Gib den Namen einer Farbe ein
- "Opacity": Der Hintergrund kann transparent dargestellt werden (Werte: 0-1, z.B. 0.4)
- "Text color": Ändern der Textfarbe
- "Styles": Feld für CSS; ACHTUNG: Immer mit ";" abschließen
- "Class": CSS-Klasse für den äußeren Container

## Blöcke bearbeiten

Um einen schon existierenden Block zu bearbeiten, fahre mit der Maus über den Block. Nun erscheint oben rechts in den Block ein Kreis mit einem Stiftsymbol. Klicke dieses Symbol an, um das Kontextmenü zu öffnen:

![Screenshot](content-flexible-pages-6.jpg)

Optionen:

- Konfigurieren: Inhalte des Blocks bearbeiten
- Manage attributes: Klassen und CSS des Blocks bearbeiten (Fortgeschritten)
- Verschieben: Den Block an eine andere Stelle verschieben
- Block entfernen: Löschen
- Control visibility: Erweiterte Anzeigeeinstellungen (Fortgeschritten)

## Blocktypen

Klicke innerhalb eines Abschnits auf "Block hinzufügen". Um eigene Inhalte einzugeben, wähle in der rechten Leiste "Benutzerdefinierter Block erstellen". Zur Auswahl stehen:

### Benutzerdefinierte Blöcke

![Screenshot](content-flexible-pages-3.jpg)

![Screenshot](content-flexible-pages-4.jpg)

Es stehem unterschiedliche Blocktypen zur Auswahl:

#### CTA (Call to Action)

Farbige Box mit Link.

Beispiel für 2 CTAs untereinander in der rechten Spalte:

![Screenshot](content-flexible-pages-5.jpg)

#### Flexible Block

Entwickler-Block / hier nicht sinnvoll nutzbar

#### HTML Block (Fortgeschritten)

Ein HTML Block enthält ein Feld für Dateien und ein Textfeld für HTML Markup. Die Dateien können im HTML Feld eingebunden werden.

<div class="admonition warning">
<p class="admonition-title">Vorsicht!</p>
<p>Sei vorsichtig mit HTML Blöcken. Wenn dein Code fehlerhaft ist, kann das dazu führen, dass deine Seite nicht mehr korrekt dargestellt wird. Am besten lege eine Test-Seite an um deinen HTML Code auszuprobieren.</p>
</div>

##### Beispiel: Bild einbinden und verlinken

- Bild hochladen per "Neue Datei hinzufügen"
- Rechtsklick auf Dateinamen des hochgeladenen Bilds
- Linkadresse kopieren und in HTML Code einfügen (ohne die Domain) `<img src="Dateipfad-hier-einfügen">`

```html
<a href="https://www.example.com"><img src="/sites/default/files/media/h4c-block-assets/2022-06/dateiname.jpg"></a>
```
<div class="admonition warning">
<p class="admonition-title">Vorsicht!</p>
<p>Die hochgeladene Bild-Datei wird nicht weiter komprimiert. Achte darauf, dass die Datei die passende Größe hat (ca. 50-300kb).</p>
</div>

#### Karte

Die Voransicht eines Artikels heißt "Karte". Mit diesem Block kannst du manuel eine Karte erstellen, ohne dass ein Artikel existieren muss. Es stehen drei Varianten zur Verfügung.

#### Normaler Block

In einem Basic block können Texte, Bilder und Dateien hinzufgefügt werden.

<div class="admonition note">
<p class="admonition-title">Bilder verlinken</p>
<p>Bilder können in Basic blocks momentan keinen Link enthalten. Bitte benutze dafür einen HTML Block.</p>
</div>

### Listen

#### Verfügbare Listen

Zu finden unter `Add block` > `Listen (Ansichten)`

Nutzbare Blöcke:

- H4C Article blocks: Card Single (Non-infinite)
- H4C Article blocks: Karte
- H4C Event blocks: Detailed list
- H4C Event blocks: Simple List
- H4C Event blocks: Taxonomy: Detailed list
- H4C Events: Detailed list facets*
- H4C Map
- H4C Map: Block Facets*
- H4C Organisation blocks (No address): Detailed list
- H4C Organisation blocks (No address): Liste
- H4C Organisation blocks (No address): Unformatierte Liste
- H4C Organisation blocks: Detailed list
- H4C Organisation blocks: Liste
- H4C Organisation blocks: Simple card random (6h)
- H4C Organisations: Detailed list facets*
- H4C Project blocks

* Die mit facets gekennzeichneten Listen haben optionale Filterblöcke unter `Facets`.

#### Listen-Einstellungen

Listen verfügen über folgende Einstellungen:

- `Titel anzeigen`: Ja/Nein (siehe unten Titel übersteuern)
- `Elemente pro Block`: Anzahl der angezeigten Einträge
- `Seitennavigation`: Immer auf Standardeinstellung `Inherit from view`
- `Titel übersteuern`: Wähle einen eigenen Listen-Titel (ganz unten)

Artikel-Listen

- `tid`: Zeige nur Einträge mit einem bestimmten Taxonomie-Begriff (Term ID eingeben, bei mehreren mit Komma getrennt)
- `nid`: Zeige nur bestimmte Einträge an (Node ID eingeben, bei mehreren mit Komma getrennt)
- `nid_1`: Zeige bestimmte Einträge NICHT an (Node ID eingeben, bei mehreren mit Komma getrennt)

Organisations-Listen

- `field_h4c_organisation_types_value`: Organisations-Typ (`all`, `initiative`, `business`)
- `tid`: Zeige nur Einträge mit einem bestimmten Taxonomie-Begriff (Term ID eingeben, bei mehreren mit Komma getrennt)

Termin-Listen

- `tid`: Zeige nur Einträge mit einem bestimmten Taxonomie-Begriff (Term ID eingeben, bei mehreren mit Komma getrennt)

<div class="admonition note">
<p class="admonition-title">Term ID (tid) und Node ID (nid)</p>
<p>Die Term ID kann in der Übersichtsliste einer Taxonomie aus dem Link zum Begriff ausgelesen werden.</p>
<p>Die Node ID kann in der Inhalts-Übersichtsliste aus dem Link zum Eintrag ausgelesen werden.</p>
</div>

### Webforms

Hinzufügen eines zuvor [erstellten Webforms](webforms.md).