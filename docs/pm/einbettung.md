# Einbettungsseiten erstellen

Erlaubt das Einbetten von Listen auf anderen Websites.

- Erstelle eine neue Seite vom Inhaltstyp "Embed page"
- Bearbeite Seite unter "Inhalt bearbeiten" (Wie [Inhalte bearbeiten: Flexible Page](content-flexible-pages.md))
- Unter "Basisdaten bearbeiten" gib ein:
    - Titel
    - Domains allowed to iframe: Gib an, welche Domain den Inhalt einbetten darf
- Um die Seite einzubetten, nutze folgenden code. Ersetze dabei die domain und node id

```
<script src="https://cdn.jsdelivr.net/gh/davidjbradshaw/iframe-resizer@v4.2.10/js/iframeResizer.min.js" type="text/javascript"></script>
<script>
document.addEventListener("DOMContentLoaded", function(){
	iFrameResize({log:false});
});
</script>
<iframe src="https://example.com/de/node/223?embed" frameborder="0" style="width:1px;min-width:100%"></iframe>

```
