# Themenseiten bearbeiten

Bestimmt Inhalte einer Themenseite können von der Redaktion (per `Basisdaten bearbeiten`) bearbeitet werden. Siehe [Redaktion > Struktur > Taxonomien > Themen](../redaktion/struktur.md#themen)

Die erweiterte Konfiguration passiert in `Inhalt bearbeiten`, ähnlich wie bei [flexiblen Seiten](content-flexible-pages.md).

<div class="admonition warning">
<p class="admonition-title">Achtung!</p>
<p>Themenseiten haben keine Revisionen. Änderungen an Inhalten und Konfigurationen können nicht rückgängig gemacht werden.<br>Entwickler Notiz: https://www.drupal.org/project/taxonomy_revisions_ui</p>
</div>

## Allgemeine Konfiguration

Beim Erstellen einer Themen-Seite wird ein Default-Layout benutzt. Dieses Layout kann je Seite angepasst werden. Änderungen am Default-Layout (nur durch Entwickler) werden dann nicht mehr übernommen.

## Konfiguration Inhaltslisten

![Screenshot](themenseiten-bearbeiten-1-Listen-Block-konfigurieren.jpg)

Einzelne Listen (`Listen (Ansichten)`) können Parameter enthalten. Wird ein solcher Parameter nicht ausgefüllt, wird er entweder automatisch befüllt (Auf Themenseiten alle `tid`) oder nicht beachtet (alle anderen).

Die Abkürzungen bedeuten:

- `tid`: Überschreibe Themen-ID (wenn leer, wird die ID der aktuell angezeigte Themenseite verwendet)
- `nid`: Only show certain content IDs (multiple IDs: "1,2,3")
- `nid_1`: Exclude certain content IDs (multiple IDs: "1,2,3")

