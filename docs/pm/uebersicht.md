# Übersicht

- PM kann Formulare erstellen und in Flexible Seiten einbetten
  - Merker: Auf contentish checkbox achten 
- PM kann Footer bearbeiten
- PM kann Blöcke bearbeiten, die Seitenübergreifend angezeigt werden (z.B. Footer, Newsletter oder Unterstützungs-Block)
- PM kann Flexible Seiten bearbeiten
- PM kann angezeigte Artikel auf Startseite ändern
- PM kann Einbettungsseiten+Einbettungscode erstellen, d.h. Einbettung von B4F Inhalten in andere Websites
- PM kann Themenseiten erstellen/bearbeiten
- PM kann User für Website Statistiken freischalten