# Webforms

<div class="admonition warning">
<p class="admonition-title">Datenschutz-Hinweis</p>
<p>Wenn persönliche Daten in Formularen erhoben werden, müssen Datenschutz-Regelungen beachtet werden. Diese betreffen z.B.</p>
<ul>
  <li>Aufklärung über Zweck und Dauer der Datenspeicherung</li>
  <li>Datensparsamkeit</li>
  <li>Einhaltung von Löschfristen</li>
  <li>Hinweis auf die Datenschutzerklärung</li>
</ul>
</div>

<div class="admonition note">
<p class="admonition-title">Dokumentation des Webform Moduls</p>
<p><a href="https://www.youtube.com/watch?v=VncMRSwjVto&list=PLXu9x8jV-0fDQz_7BXVeAbFNOoYLQhcJ1">YouTube Videos EN</a><br>
<a href="https://www.drupal.org/docs/contributed-modules/webform">Allgemeine Doku EN</a></p>
</div>

## Webforms in Hubs4Change

Unter dem Menüpunkt (...) oben rechts in der Menüleiste, findest du einen Unterpunkt "Webforms". 

- Per Klick auf "Submissions" kannst du die Einsendungen aller Webforms einsehen
- Unter Aktionen kannst du in der Webform Tabelle per Klick auf den Pfeil nach unten und dann auf "Ergebnisse" die Einsendungen des jeweiligen Webforms einsehen

### Hinweise zum Anlegen von Webforms

- WICHTIG: Beim Anlegen eines Webform muss unter `Contentish config` das Häkchen bei `Contentish` gesetzt werden. Sonst droht Datenverlust!
- Webforms sollten immer in eine Flexible Seite eingebettet werden. Siehe [Inhalte bearbeiten: Flexible Page > Blocktypen > Webforms](content-flexible-pages/#blocktypen)