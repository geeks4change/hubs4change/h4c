# Fragen und Antworten

## Wie kann Name, Logo oder Email Adresse der Plattform geändert werden?

Dies ist momentan nur durch einen Entwickler möglich.

## Wie kann ich eine Umleitung/zusätzlichen Alias einrichten?

Es gibt 2 Fälle, die wir unterscheiden:

- Fall 1: Für eine interne Seite soll ein weiterer Alias eingerichet werden (wenn z.B. 2 Seiten zusammengelegt werden sollen)
- Fall 2: Eine interne Adresse soll auf eine externe Adresse weiterleiten

Fall 1: Ich habe zwei identische Seite angelegt und beide Links wurden schon veröffentlicht. Nun möchte ich, dass alle Besuchende auf Seite 2 (url alias: `seite2`) umgeleitet werden. Seite 1 (url alias: `seite1`) soll gelöscht werden.

- Lösche Seite 1 (oder ändere zumindest den URL alias)
- Gehe auf Seite 2
- Ändere den URL Alias von `seite2` zu `seite1`
- Es wird automatisch eine Umleitung von dem alten URL Alias zu dem neuen eingerichtet
- Bei Bedarf kannst du den URL Alias wieder auf `seite2` zurück ändern. (Alle alten Aliase bleiben erhalten.)
- Wenn du alte Aliase löschen möchtest, kannst du das unter `/admin/config/search/path` tun

Fall 2: Ich möchte Umleitung auf eine externe Domain einrichten. Z.b. `domain.org/seite1` soll auf `extern.org/seite2` umleiten. 

- Gehe auf `admin/config/search/redirect`
- Klicke auf "Add URL redirect"
- In der Eingabemaske gebe unter "Pfad" `domain.org/seite1` ein und unter "An" `extern.org/seite2`
- Klicke "Speichern"
