# Mit Inhalten arbeiten

<div class="admonition note">
<p class="admonition-title">Verantwortung für Inhalte</p>
<p>Die Plattform Redaktion trägt aktuell die Verantwortung für alle veröffentlichten Inhalte. Sollten Inhalte von Usern erstellt werden, die z.B. gegen ein Gesetz verstoßen, muss die Redaktion den Inhalt entfernen sobald sie davon Kenntnis erhält. Die Redaktion ist selber dafür verantwortlich, sich über die aktuelle rechtliche Situation zu informieren.</p>
</div>

<div class="admonition note">
<p class="admonition-title">Vorsicht!</p>
<p>Flexible Seiten/Einbettungsseiten sollten nur von technisch versierten Personen bearbeitet werden. Hinweise dazu findest du im PM-Handbuch</p>
</div>

## Artikel

Artikel können nur von der Redaktion erstellt werden.

### Bilder

Es gibt ein Hero-Bild, dass oben in der vollen Breite erscheint und ein Media Image, dass rechts neben dem eigentlichen Artikel erscheint. Außerdem können im Editor Bilder hinzugefägt werden (s.u.).

### Editor

![Screenshot](inhalte-1-editor.png)

#### Besondere Formatierungen

**Box**

Erstellt eine farbig hinterlegte Box mit Text.

![Screenshot](inhalte-2-div.png)

1. Markiere Text, der in Box dargestellt werden soll, oder klicke an beliebige Stelle im Editor
1. Klicke auf das "DIV" Symbol
1. Wähle unter "Stil": Box
1. Bestätige mit OK
1. Es erscheint eine farbig hitnerlegte Box, in die Text eingegeben werden kann

**Details**

Erstellt ein ausklappbares Details Element

![Screenshot](inhalte-3-details.png)

1. Klicke in Editor
1. Klicke auf Details Element
1. Im Editor kann nun die Überschrift und der im Details Element versteckte Text eingegeben werden.

**Inline Details**

Erstellt ein Detail Element im Textfluss.

1. Gib im Editor einfach einen Text in doppelten Klammern ein `((Ich bin später versteckt))`
1. Dieser Text erscheint in der Artikel-Ansicht versteckt (siehe Screenshot)

![Screenshot](inhalte-4-inline-details.png)

Sieht in der Artikel-Ansicht so aus:

![Screenshot](inhalte-5-inline-details-ansicht.png)

#### Medien

Siehe Medienverwaltung unten.

## Einfache Seite

Eine Einfache Seite enthält ein Textfeld mit Editor. Sie eigenen sich aufgrund des Inhaltsverzeichnises besonders für lange Texte-Seiten.

### Inhaltsverzeichnis

Es kann optional ein Inhaltsverzeichnis angezeigt werden (siehe Screenshot). Das Inhaltsverzeichnis wird automatisch aus den Überschriften H2 generiert und erscheint über dem Inhaltstext.

![Screenshot](inhalte-8-page.jpg)

## Idee

User können Ideen einreichen, zu denen andere User (keine Anmeldung nötig) sie kontaktieren können. Für jede Organisation wird automatisch ein neues Team angelegt.

![Screenshot](inhalte-10-idee.jpg)

Einsehen von Kontaktaufnahmen zu veröffentlichten Ideen.

Weiteres unter "Inhalts-Einsendungen" unten.
 
## Organisation

User können Organisationen einreichen. Für jede Organisation wird automatisch ein neues Team angelegt.

Eine Organisation kann zwei Typen haben:

- Initiative
- Unternehmen

Je nachdem welcher Typ gewählt wird, können unterschiedliche Tacxonomien ausgewählt werden.

<div class="admonition warning">
<p class="admonition-title">Experimentell: Unternehmen</p>
<p>Unternehmenseinträge sind im experimentellen Stadium, d.h. der Funktionsumfang kann sich ändern oder die Funktion kann auslaufen.</p>
</div>

<div class="admonition warning">
<p class="admonition-title">Achtung!</p>
<p>Der unter "Workflow von Inhalts-Einsendungen" beschriebene Ablauf ist noch nicht für Organisationen implementiert. Für einen Workaround siehe "Teams und User-Berechtigungen".</p>
</div>

Weiteres unter "Workflow von Inhalts-Einsendungen" unten.

## Veranstaltung

User können Veranstaltungen einreichen. Veranstaltungen werden immer dem Redaktions-Team hinzugefügt.

Weiteres unter "Workflow von Inhalts-Einsendungen" unten.

## Orte

Organisationen und Veranstaltungen können Orte haben. 

Falls während der Veranstaltungs- oder Organisations-Erstellung ein Ort angegeben wird, wird dieser automatisch erstellt und mit der Organisation oder Veranstaltung verknüpft. 

### Wiederverwendbare Orte

Wenn z.B. an einem Ort mehrere Organisationen ansässig sind oder Termine stattfinden (z.B. ein Kulturzentrum) kann in der Bearbeiten-Ansicht die Checkbox bei "Wiederverwendbar" gesetzt werden. Nun kann der Ort an mehreren Organisationen/Veranstaltungen verknüpft werden.

## Ablauf: Einsendungen

Folgende Inhalte können von nicht angemeldeten Nutzenden eingereicht werden:

- Organisation
- Projekt/Ideen
- Termin

### Allgemeiner Ablauf

- Redaktion bekommt Email-Benachrichtigung mit Link zu Einsendung (WICHTIG: Bevor Redaktion Inhalte freischalten kann, muss User Email Adresse bestätigen - außer bei Terminen, s.u.)
- Redaktion schaltet Einsendung frei
- Inhalte und User account werden erstellt; User wird über seinen Zugang benachrichtigt
- Nach der Freischaltung sieht User die neuen Inhalte in seinem [User Dashboard](teams.md#user-dashboard) 
- Weitere User können einem Team zugeordnet werden, siehe [User zu Gruppen hinzufügen](teams.md#user-zu-gruppen-hinzufugen)
- Sonderfall Redaktionsmitglieder: Freischaltung der Einsendung automatisch
- Sonderfall eingeloggte User: Email bestätigen nicht notwendig

Eine Übersicht aller Einsendungen befindet sich unter: `/de/content-overview/webform4content`

![Screenshot](img-user-dashboard.jpg)

### Organisations-Einsendung

Folgendes geschieht bei der Freischaltung einer Einsendung:

- Organisation wird erstellt
- User account wird erstellt
- Team wird erstellt
- Organisations- und User-Eintrag wird Team zugeordnet
- Optional: Ort wird erstellt (und in der Organisation referenziert)

### Projekt/Idee-Einsendung

Folgendes geschieht bei der Freischaltung einer Einsendung:

- Projekt/Idee wird erstellt
- User account wird erstellt
- Team wird erstellt
- Projekt/Idee- und User-Eintrag wird Team zugeordnet

### Termin-Einsendung

Folgendes geschieht bei der Freischaltung einer Einsendung:

- Termin wird erstellt
- Termin wird dem "Community Termine"-Team zugeordnet (von Redaktion verwaltet)
- Optional: Ort wird erstellt (und in der Termin referenziert)

Hintergrund: Die Termin-Einreichung soll möglichst niederschwellig erfolgen, daher wird kein User-Account angelegt. Perspektivisch sollen Termine Organisationen zugeordnet werden können.

Wichtig: Falls User eingeloggt ist, kann es **die selbst erstellten** eingereichten Termine bearbeiten, siehe `Ablauf: Änderungen`.

## Ablauf: Änderungen an Inhalten

Über das User Dashboard können User Änderungen an Inhalten vornehmen. Diese Änderungen müssen von der Redaktion jedoch freigeschaltet werden. Der Ablauf ist:

- User bearbeitet Inhalt und reicht Änderungen zur Prüfung ein
- Redaktion bekommt Email-Benachrichtigung
- Redaktion veröffentlicht Änderungen

Redaktionsmitglieder können Inhalte direkt ändern und veröffentlichen.

Eine Übersicht aller zu moderierender Inhalte befindet sich unter `/de/admin/content-overview/moderated`

## Revisionen von Inhalten

Beim Aktualisieren eines Inhalts wird eine neue Revision angelegt. D.h. jede ältere Version eines Inhalts bleibt erhalten und der Inhalt kann zu ihr zurückgesetzt werden.

Zur Revisionsübersicht gelangst du, wenn du einen Inhalt aufgerufen hast, rechts auf das Kontextmenü klickst.

![Screenshot](inhalte-9-revision.jpg)

Alternativ: 

1. Die URL ist `node/ID/revisions`. 
1. Die ID kannst du in der Adresszeile auslesen, wenn du einen Inhalt bearbeitest.

## Medien

### Einbettbare Medien

In vielen Inhaltstypen können Medien hochgeladen oder eingebettet werden.

#### Bilder

![Screenshot](inhalte-6-image-embed.png)

*Pfichtfelder beim Hochladen von Bildern* 

- Alternativ-Text (z.B. für Menschen, die das Bild nicht sehen können)
- Rechteinhaber
- Persönlichkeitsrechte

Die Felder in dieser Maske können später nur über die Medienverwaltung bearbeitet werden (siehe Abschnitt zur Medienverwaltung).

*Einstellungen*

Nachdem das Bild hochgeladen wurde, erscheint ein Dialog für die Bild-Einstellungen:

- Größe: Klein (ca 1/4 der Spaltenbreite), Mittel (ca 2/5), Volle Breite
- Für Klein und Mittel kann sinnvollerweise eine Ausrichtung eingestellt werden, wird links oder rechts gewählt, fließt der nachfolgende Text auf der anderen Seite um das Bild herum

Die Felder in dieser Maske können später noch per Doppelklick auf das Bild im Editor geändert werden.

<div class="admonition note">
<p class="admonition-title">Bildgröße im Editor</p>
<p>Die Größe des Bildes ist in der Bearbeiten-Ansicht immer gleich. Beim Betrachten des Artikels wird jedoch die richtige Größe angezeigt.</p>
</div>

<div class="admonition note">
<p class="admonition-title">Verlinkung von Bildern</p>
<p>Bilder können momentan nicht mit einem Link versehen werden.</p>
</div>

#### Video, Audio und ander Dateien

![Screenshot](inhalte-6b-video-audio.png)

Es können folgende weitere Medien genutzt werden:

- Einbettung von youtube/vimeo Videos
- Hochgeladene Video-Dateien
- Hochgeladene Audio-Dateien
- Andere Dateien (txt pdf doc docx xls xlsx ppt pps pptx odt ods odp)

Eingebettete Inhalte von externen Diensten (youtube/vimeo) werden dem user standardmäßig nicht angezeigt. User muss diese Inhalte erst freischalten.

Hochgeladene Audio-/Video-Dateien werden vom nativen Player des jeweiligen Browsers abgespielt.

![Screenshot](inhalte-7-blocked-video.png)

### Medienverwaltung

Eine Übersicht über alle Medien findet sich unter dem Redaktions-Menüpunkt `(...)` > Medien.

<div class="admonition note">
<p class="admonition-title">Mehrfachverwendung von Medien</p>
<p>Medien können aktuell nicht auf einfachem Weg mehrfach verwendet werden. Als Workaround kann der html code einer Medien-Einbettung kopiert werden (siehe Beispiel unten). ACHTUNG: Falls ein Medium über die Medienverwaltung verändert wird, ändert es sich in allen Einbettungen.</p>
</div>

```
<drupal-entity data-embed-button="h4c_media_image_embed" data-entity-embed-display="view_mode:media.h4c_medium" data-entity-embed-display-settings="" data-entity-type="media" data-entity-uuid="94aedf94-294a-4dff-a5db-66ee8c565512" data-langcode="de" />
```