# Plattform Struktur

## Menüs

In der Regel gibt es zwei Menüs:

- Hauptmenü
- Fußleisten-Menu

<div class="admonition note">
<p class="admonition-title">Abweichungen</p>
<p>bonn4future.de: Das genutzte Fußleistenmenü befindet sich nicht unter Menüs, sondern in einem besonderen Block. Siehe Technik-Handbuch</p>
</div>

### Menüpunkte

Unter Menüs > Hauptmenü gibt es folgende Funktionen:

- Reihenfolge einstellen
- Neue Menüpunkte erstellen
- Menüpunkte deaktivieren 
- Menüpunkte bearbeiten oder löschen 

![Screenshot](struktur-1.png)

<div class="admonition note">
<p class="admonition-title">Abweichungen</p>
<p>bonn4future.de: Aufgrund der Multi-Domain Funktion muss ein Menülink immer zu einer Seite zeigen, die der Domain zugeordnet ist. Ist dies nicht der Fall und ein Menüpunkt zeigt z.B. auf einen externen Link oder ein Webform (ohne das es in eine Flexible Seite eingebettet wurde), ist der Menüpunkt auf allen Domains sichtbar</p>
</div>

### Menüpunkt bearbeiten

#### Erläuterungen zu den einzelnen Einstellungen

**Link**

Hier kann eine Plattform-Seite per Suche ausgewählt werden oder ein Link direkt eingegeben werden. Falls eine Seite auf der Plattform nicht gefunden werden kann und der Link direkt eingegeben wird, bitte immer nur den relativen Pfad eingeben. D.h.:

- Nicht *https://www.bonn4future.de/de/organisationen*
- Sondern nur den letzten Teil: */de/organisationen*

Links zu externen Seiten werden komplett eingegeben.

**Als ausgeklappt anzeigen**

Wenn ein Menüpunkt Unterpunkte hat, muss diese Option aktiviert sein. Sonst werden die Unterpunkte nicht im Menü angezeigt.

**Experimentel: FontAwesome Icon**

Diese Funktion wird voraussichtlich in Zukunft entfernt.

![Screenshot](struktur-2.png)

### Menüpunkt mit Inhalt erstellen/bearbeiten

Für bestimmte Seitentypen können Menüpunkte direkt beim Erstellen des Inhalts angelegt werden:

- Einfache Seite
- Flexible Page

Siehe auch Abschnitt "Mit Inhalten arbeiten"

## Taxonomien

### Übersicht

Jede Taxonomie gehört üblicherweise zu einem Inhaltstyp. Eine Ausnahme bilden **Themen** und **Schlagworte**.

![Screenshot](struktur-3-taxonomie.png)

**Wichtig**: Nur Schlagworte können ad-hoch erstellt werden, also während der Erstellung eines Inhalts. Begriffe in allen anderen Taxonomien müssen vorher von der Redaktion angelegt werden.

### Themen

Themen können nur von der Redaktion angelegt werden. Jedes Thema ist gleichzeitig eine Themenseite. Auf einer Themenseite werden dargestellt: 

- Automatisch: Unterschiedliche Listen von Inhalten: Organisationen, Artikel, Termine 
- Benutzerdefinierte Texte per "Basisdaten bearbeiten": Die Beschreibung erscheint direkt unter der Überschrift links neben den Organisationen, das Hero-Bild erscheint oben auf der Seite

![Screenshot](struktur-4-thema-basisdaten.png)

### Schlagworte

Schlagworte können von allen Usern eingegeben werden. Mit ihrer Hilfe können z.B. Inhaltslisten gefiltert werden (siehe Technik-Handbuch)

### Welche Taxonomien werden wo genutzt

- Themen (Artikel, Veranstaltungen, Organisationen)
- Freie Schlagworte (Artikel, Veranstaltungen, Organisationen)
- 17 Ziele (Organistionen)
- Branchen (Organisationen, nur wenn als Organisationstyp "Unternehmen" ausgewählt ist)
- Nachhaltigkeit (Organisationen, nur wenn als Organisationstyp "Initiativen" ausgewählt ist)
- Experimentell: Aktionsfelder (Organisationen)
- Veranstaltungs-Typen (Veranstaltungen)
- Artikel-Typen (Artikel)
