# Teams und User-Berechtigungen

Hubs4Change basiert auf Teams (teilweise auch Gruppen genannt, dies ist die alte Bezeichnung). User sind immer Mitglieder in einem oder mehreren Teams. Aus dieser Mitgliedschaft leitet sich die Berechtigung von Inhalten ab. D.h. auch, dass der Autor eines Inhalts keine anderen Rechte hat, als ein anderes Mitglied der Gruppe, zu der der Inhalt gehört.

- Beispiel 1: In der Redaktionsgruppe sind alle User Mitglied, die Zugriff auf die redaktionell gepflegte Plattform-Inhalte haben.
- Beispiel 2: Wenn ein User eine Idee erstellt, wird automatisch ein Team erstellt. So ist es möglich, dass auch andere User die Idee bearbeiten können, sofern sie Mitglied der Gruppe werden.

**Wichtig**: Die Redaktions-Mitglieder haben weitere Berechtigungen, die sich aus globalen Rollen ergeben und nicht nur aus der Gruppenzugehörigkeit 

<div class="admonition warning">
<p class="admonition-title">Wichtig!</p>
<p>Bei Organisationseinträgen passiert dies aktuell nicht automatisch. Eingereichte Termine werden der Redaktionsgruppe hinzugefügt.</p>
</div>

## User Dashboard

Im Dashboard sehen User alle Inhalte, für die sie (per Gruppenzugehörigkeit) Bearbeiten-Rechte haben:

![Screenshot](teams-1-user-dashboard.jpg)

## User zu Gruppen hinzufügen

- User meldet sich per Email bei Redaktion mit Bitte Zugriff auf Inhalte zu bekommen
- Falls noch nicht geschehen, muss sich User registrieren per `/de/user/register`
- Redaktion schaltet User frei (keine globale Rolle auswählen)
- Redaktion fügt User manuell der Gruppe hinzu, per `/de/group/ID/members`
- Die Gruppen-ID kann per `/de/admin/content-overview` aus dem Link in der Spalte Group ausgelesen werden
- Beim Hinzufügen die Checkbox bei `Group Admin` setzen 

<div class="admonition warning">
<p class="admonition-title">Wichtig!</p>
<p>Ob ein User Zugriff auf bestimmte Inhalte bekommen darf muss die Redaktion entscheiden, ggflls in Rücksprache mit einem aktuellen Gruppenmitglied.</p>
</div>