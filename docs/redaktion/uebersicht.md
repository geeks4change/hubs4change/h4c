# Übersicht

**Struktur der Plattform**

- Redaktion kann Menüpunkte erstellen und bearbeiten
- Redaktion kann Taxonomien bearbeiten und Begriffe Inhalten zuordnen oder entfernen

**Teams und User-Berechtigungen**

- Redaktion kann User zu Teams hinzufügen

**Mit Inhalten arbeiten**

- Redaktion kann Inhalte erstellen und bearbeiten
- Redaktion kann Bilder und Videos in Artikel, Veranstaltung, Einfache Seite einbetten
- Redaktion kann Inhalts-Einsendungen freischalten
- Redaktion kann Inhalts-Revisionen einsehen und zurücksetzen
- Redaktion kann Medien bearbeiten

**Verschiedenes**

- Redaktion kann Webform Einsendungen sehen
- Redaktion kann Website Statistiken einsehen

**Fragen und Antworten**

- Wie kann Name, Logo oder Email Adresse der Plattform geändert werden?
- Wie kann ich eine Umleitung/zusätzlichen Alias einrichten?