# Verschiedenes 

## Kalender/Organisations-Seiten auf anderen Websites einbetten (Für bonn4future.de)

Unten stehen die Schritte zur Einbettung bestehender Kalender-/Kartenseiten für Bonn im Wandel. Allgemeine Hinweise zur Erstellung von Einbettungsseiten stehen im [Plattform Manager Handbuch](../pm/einbettung).

### Kalender einbetten

- Domain ergänzen: https://dev.bonn4future.de/de/node/5028/edit
- Einbettungscode:

```
<script src="https://cdn.jsdelivr.net/gh/davidjbradshaw/iframe-resizer@v4.2.10/js/iframeResizer.min.js" type="text/javascript"></script>
<script>
document.addEventListener("DOMContentLoaded", function(){
  iFrameResize({log:false});
});
</script>
<iframe src="https://example.com/de/node/5028?embed" frameborder="0" style="width:1px;min-width:100%"></iframe>

```

### Karte einbetten

- Domain ergänzen: https://dev.bonn4future.de/de/node/4616/edit
- Einbettungscode: 

```
<script src="https://cdn.jsdelivr.net/gh/davidjbradshaw/iframe-resizer@v4.2.10/js/iframeResizer.min.js" type="text/javascript"></script>
<script>
document.addEventListener("DOMContentLoaded", function(){
  iFrameResize({log:false});
});
</script>
<iframe src="https://example.com/de/node/4616?embed" frameborder="0" style="width:1px;min-width:100%"></iframe>

```

## Matomo Statistiken

Die Statistiken sind unter https://matomo.machbarmacher.net/ einsehbar.

Für einen Benutzeraccount wende dich bitte an einen Entwickler.

Das Matomo Handbuch findest du unter https://matomo.org/guide/getting-started/getting-started/

## Newslettertool

Das Newsletter-Tool erleichtert es Redaktionen, die Email-Newsletter erstellen Inhalte aus der Plattform zu nutzen.

Das Newslettertool befindet sich unter: `/de/copy-list`

### Webforms in Seiten einbinden

Siehe "Flexible Seiten bearbeiten" im PM-Handbuch.

### Werbform Einsendungen einsehen

#### Je Webform

Redaktionsmitglieder können die Einsendungen zu Webforms über das Kontextmenü des Webforms erreichen:

![Screenshot](inhalte-10-idee.jpg)

#### Alle

Alle Webform-Einreichungen sind unter `/de/admin/structure/webform/submissions/manage` einsehbar.