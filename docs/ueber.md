# Was ist Hubs4Change

Hubs4Change ist ein Redaktionssystem für Netzwerk-Organisationen.

## Hubs4Change als Netzwerk-Hub

Hubs4Change macht Netzwerke sichtbar und stellt den Netzwerk-Mitgliedern wirkungsvolle Online-Tools zur Verfügung.

Hubs4Change versteht sich als Netzwerk aus Organisationen, die den Wandel in ihrer Region sichtbar machen. Features, die für einen Hub entwickelt werden, stehen allen Hubs zur Verfügung. Dokumentation, die von einem Hub geschrieben wird, vereinfacht die Arbeit von allen. Usw.

Hubs4Change ist eine Drupal 8 Distribution.

## Hubs4Change 3.0.x Sites

- [bonn4future.de](https://www.bonn4future.de/de)

## Hubs4Change 2.0.x Sites

- [StadtWandler Freiburg](https://www.stadtwandler.org)
- [Nordosten baut grün](https://www.der-nordosten-baut-gruen.de/de)
- [SENS Social Entrepreneurship Network Suisse](https://www.bonn4future.de/de)

## Geeks4Change

Hubs4Change wird entwickelt von den [Geeks4Change](https://www.geeks4change.net).