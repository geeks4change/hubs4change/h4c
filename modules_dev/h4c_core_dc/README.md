# About default content (dc)
- dc content modules are suffixed with `_dc` of the module name it adds default content to

# Creating/updating dc
- We rely on the patch from [Do not reimport existing entities [#2698425] | Drupal.org](https://www.drupal.org/project/default_content/issues/2698425)#122 to not have to worry about duplicate dc - which happens quickly e.g. if you export dc with references with the same terms in different modules
- To get file names added modules content folfer to include into info.yml file run `bin/h4c-dc-list ./module_dev/h4c_organisation_dc` and copy-paste output
- See also https://gitlab.com/geeks4change/hubs4change/h4c/issues/117

# Known issues
- `[warning] Invalid display settings encountered. Could not process following settings for entity type "media" with the uuid "f0333d00-75aa-4e01-9bb0-6ddfb2ae0ea9": `
  - Unknown why this happens, content should be installed correctly non-the-less

