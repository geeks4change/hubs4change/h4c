# H4C Content Moderation actions

This was moved from h4c_core to a seperate module to avoid errors on config distro update. Did not note or investigate further on the error.

## Patch

- `"h4c#472 / 2797583 / bulk content moderation actions": "https://www.drupal.org/files/issues/2020-01-25/2797583-149.patch"`