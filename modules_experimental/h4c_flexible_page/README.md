# H4C Flexible page

## Use case
- Let devs prototype on the fly layout builder pages which are later moved into indivdual stable modules

## Caution
- This module will most likely always remain experimental
- Use cases developed with the help of this module are rolled out as individual modules or added to existing modules
- Access to layout is not meant to be given to editors or platform managers
- If needed they can use h4c_body field to control some content on those pages

## TODO
- Keep track of all flexi pages used across sites and blocks therein, currently only used for site/portal frontpages