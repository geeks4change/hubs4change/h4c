# H4C Site settings

Installs a settings page with a single field which is used as a metatag fallback image for pages that do not have an image field. Otherwise the logo would be used which is not suitable for social media previews in most cases.

This modules shall help to cover two use cases:
- 1. Let platform managers configurable existing fields like the site name in one place
- 2. Let platform managers configurable new fields which can be used e.g. as token (e.g. the already existing image field used in h4c_metatags module)

## Issues
- [Module review: site settings / config_pages (#278) · Issues · geeks4change / Hubs4Change / H4C Module suite · GitLab](https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/278)
- [Allow users to easily add important metatags (#449) · Issues · geeks4change / Hubs4Change / H4C Module suite · GitLab](https://gitlab.com/geeks4change/hubs4change/h4c/-/issues/449)
- [Niedrig hängende SEO Maßnahmen, z.B. Überschrift auf allen Seiten (#215) · Issues · geeks4change / Sites / site-h4c-freiburg · GitLab](https://gitlab.com/geeks4change/sites/site-h4c-freiburg/-/issues/215)
