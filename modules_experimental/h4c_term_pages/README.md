# H4C Term pages
- For topics/tags

## Future requirements
- Make it possible to customize term pages 
  - per site for all pages
  - per taxonomy
  - per term
- Review as alternative to LB: [Library Layouts Auto Selection & Selection Rules Like It Was In Panels (D7) for Page Variants [#3089493] | Drupal.org](https://www.drupal.org/project/layout_library/issues/3089493)

## Current implementation
- We create a page_manager page `/taxonomy/term/{taxonomy_term}` for topics and tags
- We create a `/topics` views page (to display topics as simple card), which we deprecate at some point, until then you need to create an alias `/topics` => `/themen`, if you want the page to be linked in breadcrumbs
- For tags there is no `/tags` page, it appears in breadcrumbs without link
- Make sure that aliases are generated for terms so the breadcrumb work correctly, e.g. check `Generate automatic URL alias` for each term if not
- :warning: Make sure that your terms and vocabulary names are translated properly, de term aliases depend on it!