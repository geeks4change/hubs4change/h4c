# H4C Config Management 

## Goal 

Update config delivered by modules on a site.

## Known Issues
- Config distro ignores config deletions [h4c/issues/147](https://gitlab.com/geeks4change/hubs4change/h4c/issues/147)
- Config distro runs although not all dependencies can be resolved [h4c/issues/212](https://gitlab.com/geeks4change/hubs4change/h4c/issues/212)
- Missing config_distro import button [h4c/issues/151](https://gitlab.com/geeks4change/hubs4change/h4c/issues/151)
- It is hard to keep an overview of which config is changed by the site and therfore not affected by upstream changes anymore
  - Workaround: Check in features_ui does not help much, because it is cluttered by false config differences
  - It would be awesome to have a "Reset to upstream" option
- Config already present in other modules is not altered by config_distro, see [h4c/issues/150](https://gitlab.com/geeks4change/hubs4change/h4c/issues/150)
  - Workaround: Use config_actions instead
- [Occasional missing dependencies after config actions (provider) run](https://gitlab.com/geeks4change/hubs4change/h4c/issues/155#note_181760935)
  - Workaround: Check git and do not commit

## Configuration Synchronizer

[config_sync](https://www.drupal.org/project/config_sync) compares your active sites configuration with your codebase. With snapshots taken on changes in active config, config_sync allows you to merge in config update without ovewriting customizations.

## Configuration Distro

[config_distro](https://www.drupal.org/project/config_distro) "provides a framework for managing configuration updates from distributions". [Visit on your site](/en/admin/config/development/configuration/distro).

## Config Distro Ignore

Config can be excluded from being updated by config_distro. [Visit on your site](/en/admin/config/development/configuration/distro/ignore)

## Config Actions (Provider)

Config Actions are used to add to or alter config in existing config files. We use this in a few cases:

### 1. Avoid circular dependencies
- [workflows.workflow.editorial](/en/admin/config/workflow/workflows/manage/editorial)
- [search_api.index.h4c_index](/en/admin/config/search/search-api)

### 2. Alter core/contrib config
- Don't use the administration theme when editing or creating content [h4c_core/config/actions/node.settings.yml](/en/admin/appearance)
- Turn off drupal core aggregation in favor off AdvAgg [h4c_core/config/actions/system.performance.yml](/en/admin/config/development/performance)
- Set registration to require admin approval [h4c_core/config/actions/user.settings.yml](/en/admin/config/people/accounts)
- Set devel_dumper to var_dumper [h4c_devtools/config/actions/devel.settings.yml](/en/admin/config/development/devel)
- Set error level verbose [h4c_devtools/config/actions/system.logging.yml](/en/admin/config/development/devel)
- ... and others (search for actions folders in H4C)

### Hint: Snapshots get changed by actions in other modules
- config_actions are regularly used to add/change config of one module *by* another module, e.g.:
  - Add action to h4c_organisation_dc which alters config from h4c_organisation
  - h4c_organisation_dc does not have any other config than the action
  - Snapshot of h4c_organisation changes, we see an empty snapshot of h4c_organisation_dc
  - This is potentially confusing
