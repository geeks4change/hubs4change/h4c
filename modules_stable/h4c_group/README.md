# About H4C Group

## Goal

Create a group type(s) which serve as the primary content access regime in H4C.

## Group Types

The Hubs4Change [Group Configuration](/admin/group/types) is based on the following modules: 

- [Group Behavior module](https://www.drupal.org/project/group_behavior)  ([Organisation](/admin/group/content/manage/group_content_type_47270b0052668)/[Campaign](/admin/group/content/manage/group_content_type_2425d3734d7e8)) makes content types behave like groups in that it creates a group on node creation (see content plugin settings) and adds the created node to the group

***Each content can only be added to a single group. A group can only have a single Groupbehavior node.***

### Organisation Group Type

- Group Behaviour = Autocreate group on organisation node creation
- Groupcontent: Articles, Event, Location, Media
- See [Configure available content](/admin/group/types/manage/h4c_organisation_group/content)

### Editorial Group Type

- No Group Behavior
- Group Content: Article, Event, Location, Basic Page, Media
- All general Website Content goes into an editorial group (there might be one for each domain, but this is not [decided](https://gitlab.com/geeks4change/drutopiadev/issues/619#3-spezialfall-redaktionsgruppe))
- Event Submissions from anonymous users are saved as draft here
- See [Configure available content](/admin/group/types/manage/h4c_editorial_group/content)

#### Why an Editorial Group Type?

##### We might not want to use this!

- We want users to join the editorial team without having access to all of the site's content items
- Media Integration:
  - The [Media Browser](http://master.hubs4change.mbm.wod.by/en/admin/structure/views/view/media_entity_browser?destination=/en/admin/structure/views) has a group filter, if there is none, no media will be shown
  - In Groups we don't want to show the media items of the editorial team (This would happen if we had no Group for the Editorial Team and broaden the filter to Group X + Media without Group, which would be neccessary if nodes without group shall be able to share a media library)
- We might want to have content which cannot be edited be the Editorial Team (which is created by admins with no group)

#### Why not use an Organisation Group as Editorial Group(s)?

- We want to have the possibility to have different group content and group roles for the editorial group
  - Group Content: The Editorial group content only differs in "Basic Page" which we might want to introduce to Organisation Groups as well at some point (see [#444 [Konzept] Unterseiten für Organisationen](https://gitlab.com/geeks4change/drutopiadev/issues/444) )
  - Roles: We only have Group Admin atm, we *might* be happy with the same group roles for organisation and editorial

#### Problems with Editorial Group Type

- If we decide to add content to domains based on the Groupbehavior Node, we need a seperate solution for Editorial Group, because it has ne Group Behavior ( see [#526 Set domain_access values](https://gitlab.com/geeks4change/drutopiadev/issues/526))
- It's a seperate Group Type to maintain

### Discussions
- Editorial Group type see [#429 Create Editorial Group Type](https://gitlab.com/geeks4change/drutopiadev/issues/429)**
- Select/show Parent Group on Groupcontent Nodes for evaluation [geeks4change/drutopiadev/issues/228]](https://gitlab.com/geeks4change/drutopiadev/issues/228)

### Deprecated in #450
- [Gmedia patch](https://www.drupal.org/project/group/issues/2984315) let's you attach media items to groups
- [GroupMediaPlus module](https://www.drupal.org/project/groupmediaplus) adds media items automatically to the group of the node to which the media is added
