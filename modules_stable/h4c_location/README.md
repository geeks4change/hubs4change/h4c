# About H4C Location

## Goal

Create a content type for reusable locations

## Status
- Locations are owned by platform (managers)
- Any user can 
  - create locations via the organisation or event form (and mark as reusable location or not)
  - choose a location from the list of reusable locations
  - CANNOT edit it after creation
- Only platform managers can edit or delete locations
  - they are encouraged to check if the location is used somewhere before doing so
  - they are encouraged to keep the list of reusable locations in good shape
- Locations are non group content

## Todo
- (?) Add message to location (form?) for users with permission to edit/delete: "Before editing or deleting a location please check if it is used by any organisation or event and notify those groups about your change. You find the events and organisations on the location page." (e.g. https://www.stadtwandler.org/de/locations/oekostation-freiburg)
- (1-2 days) Moderate reusable setting, so that:
  - anyone can create non-reusable locations + propose as reusable
  - if proposed as reusable, the reusable=true version is saved only as draft, and admin can moderate this

## Resources
- Clarify location ownership #451