# About H4C Metatag

## Goal
- Set up basic settings for facebook/twitter (#380)
- Let editors add basic metatags easily (Prototype, see #449)

## Todo
- Review `metatag_defaults.global`, especially that it technically should require h4c_site_settings
- Update `metatag_defaults.node` once h4c_metatags paragraph is roled out to all content types

## Known issues
- Facebook link preview: If the title is longer than 1 line, the description won't show. This seems to be the way fb handles it and not a metatag settings issue (as of 03.08.2020)

## Resources
- Explicitly declare image dimensions to help fb/twitter fetch preview at first try
  - https://developers.facebook.com/docs/sharing/webmasters/images/
  - https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/summary-card-with-large-image