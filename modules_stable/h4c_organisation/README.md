# About H4C Organisation

## Goal

Create a content type for organistation profiles that can be of type business and/or initiative and has
- a logo
- a location
- contact information
- opening hours
- header slideshow
- taxonomy terms: topics (initiative), business types (business), sustainability (business), tags

This is a group behaviour content type

## It provides
- Organisation views page `/organisations`
- Organisation views blocks (*TODO* for each view mode) 