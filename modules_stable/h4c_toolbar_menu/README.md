# H4C Toolbar menu

## Status
- `Add content`/`Manage content` menu links/local tasks/actions are provided by modules
- Admin content views are provided by modules
- `Taxonomy` and `Menu` menu links are generated automatically for each item
- :warning: h4c_content & h4c_content_moderated (as fundumantal) config for this module live inside optional bc of group dependency

## Gin horizontal toolbar menu
- For improved and simplified UX `h4c_gin` subtheme enables horizontal toolbar for all users

### Secondary menu
- secondary menu now lives on the right side below admin toolbar *only* on pages rendered with gin theme
- secondary menu is not visible on all gin pages [Secondary toolbar is not visible on node forms [#3176607] | Drupal.org](https://www.drupal.org/project/gin/issues/3176607)
- admin_toolbar_search is not functional if displayed in gin secondary menu (alternative: coffee)
- ~~role_toggle (with form) and devel are displayed as dropdown in secondary menu~~
- ~~role_toggle is also displayed under user profile dropdown to be accessible from enzian pages~~ currently role_toggle is under development and not supported
- User menu and back to site buttons are duplicate in secondary menu (visible only for platform managers)

### Drawbacks
- Users only ever see one toolbar at a time and cannot switch to another toolbar (admins)
- Both menus are rendered if user has permission to see admin menu and h4c_toolbar_menu
  - :warning: admins "see" only the admin toolbar bc it is layered on top of the other, which is a bit hacky but seems to be stable enough [Add possibility to use a different administration menu configurable per role as the Gin toolbar [#3169570] | Drupal.org](https://www.drupal.org/project/gin_toolbar/issues/3169570)

### How to switch back to classic toolbar
- Globally in H4C Gin theme settings
- Per user in user edit form IF h4c_gin is used to display it (in H4C enzian is used bc gin exposes to many options there) 
  - [Add permissions for User settings [#3138529] | Drupal.org](https://www.drupal.org/project/gin/issues/3138529)

## How modules can add menu links/local tasks
- Any module can add menu links or local tasks to `h4c-toolbar-menu` menu, e.g. see `h4c_organisation` or `h4c_user`
- Weight convention: 
  - `-100` is reserved for items that shall always be positioned first
  - `100`is reserved for items that shall always be positioned last
